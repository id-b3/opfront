/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <sstream>
#include "opfront/multi_surface.h"
#include "opfront/mainloop.h"
#include "util/camera.h"

static const int window_height = 600, window_width = 600;
static int last_x = window_width/2, last_y = window_height/2, draw_outer = 0,
    loop_count = 0;
static bool enable_lighting = false, wireframe = false, draw_flow_lines = true,
     draw_balls = false, infinite_loop = false, mouse_look = false;
static camera c;

// Internal stuff.
static double camera_x_pos = 0, camera_y_pos = -300, camera_z_pos = 0,
       walk = 0, strafe = 0, roll = 0, yaw = 0, pitch = 0,
       max_distance = 1;

// Opfront variables
static GtsSurface *surface;
static opfront *segmentor;
static int front_edge_links, maximum_front_size, opfront_index = 0;
static image_tracer_type *image_tracer;

gint
multi_render_face(gpointer item, gpointer data)
{
    GtsFace *f = static_cast<GtsFace*>(item);
    GtsVertex *v0, *v1, *v2;
    gdouble x, y, z;
    gts_triangle_vertices(&f->triangle, &v0, &v1, &v2);
    gts_triangle_normal(&f->triangle, &x, &y, &z);
    gdouble l = sqrt(x*x+y*y+z*z);
    glNormal3f(x/l, y/l, z/l);
    int n = segmentor->front_number(f);
    if(n != -1) glColor3f((n%2+1)/2, (n%5+1)/5.0f, (n%7+1)/7.0f);
    else if(GTS_MVERTEX(v0)->moving && GTS_MVERTEX(v1)->moving &&
            GTS_MVERTEX(v2)->moving) glColor3f(1.0, 0.3, 0.1);
    else glColor3f(0.3, 0.1, 1.0);
    glVertex3dv(static_cast<gdouble*>(&v0->p.x));
    glVertex3dv(static_cast<gdouble*>(&v1->p.x));
    glVertex3dv(static_cast<gdouble*>(&v2->p.x));
    return 0;
}

void
render_vertex(GtsVertex *v)
{
    GtsVertex *m = GTS_VERTEX(v);
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(m->p.x, m->p.y, m->p.z);
}

gint
render_face(gpointer item, gpointer data)
{
    GtsFace *f = static_cast<GtsFace*>(item);
    GtsVertex *v0, *v1, *v2;
    gdouble x, y, z;
    gts_triangle_vertices(&f->triangle, &v0, &v1, &v2);
    gts_triangle_normal(&f->triangle, &x, &y, &z);
    gdouble l = sqrt(x*x+y*y+z*z);
    glNormal3f(x/l, y/l, z/l);
    render_vertex(v0);
    render_vertex(v1);
    render_vertex(v2);
    return 0;
}

void
draw_lines()
{
    GtsSurface *sphere = gts_surface_new(gts_surface_class(),
        gts_face_class(), gts_edge_class(), gts_vertex_class());
    gts_surface_generate_sphere(sphere, 1);
    for(int i = 0; i < segmentor->column_count(); i++)
    {
        if(draw_balls)
        {
            for(int j = 0; j < segmentor->column_length(i); j++)
            {
                glPushMatrix();
                const gdouble *v = segmentor->column_position(i, j);
                glTranslatef(v[0], v[1], v[2]);
                glScalef(0.1, 0.1, 0.1);
                glBegin(GL_TRIANGLES);
                gts_surface_foreach_face(sphere, render_face, 0);
                glEnd();
                glPopMatrix();
            }
        }
        // OpenGL will segfault if no vertices send between glBegin and
        // glEnd.
        if(segmentor->column_length(i) < 2) continue;

        glBegin(GL_LINES);
        ofloat l = 0;
        for(int j = 0; j < segmentor->column_length(i)-1; j++)
        {
            const gdouble *c0 = segmentor->column_position(i, j),
                          *c1 = segmentor->column_position(i, j+1);
            l += linalg_dis_ve3(c0, c1);
        }
        if(!l) continue;
        ofloat k = 0;
        for(int j = 0; j < segmentor->column_length(i)-1; j++)
        {
            const gdouble *v0 = segmentor->column_position(i, j),
                          *v1 = segmentor->column_position(i, j+1);
            glColor3f(k/l, (l-k)/l, k/l);
            glVertex3f(v0[0], v0[1], v0[2]);
            k += linalg_dis_ve3(v0, v1);
            glColor3f(k/l, (l-k)/l, k/l);
            glVertex3f(v1[0], v1[1], v1[2]);
        }
        glEnd();
    }
    gts_object_destroy(GTS_OBJECT(sphere));
}

void
update()
{    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    c.update();
    glPushMatrix();
    glColor3f(1.0, 0.0, 0.0);
    if(draw_outer) multi_surface_select(surface, draw_outer);
    glBegin(GL_TRIANGLES);
    gts_surface_foreach_face(surface, multi_render_face, 0);
    glEnd();
    if(draw_outer) multi_surface_select(surface, 0);
    if(draw_flow_lines) draw_lines();
    glPopMatrix();
    SDL_GL_SwapBuffers();
}

void
mouse_motion(int x, int y)
{
    int delta_x = x-last_x, delta_y = y-last_y;
    last_x = x; last_y = y;
    c.yaw(delta_x*0.01);
    c.pitch(delta_y*0.01);
}

void
init_gl()
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_Surface* screen = SDL_SetVideoMode(window_width, window_height, 16,
                                           SDL_OPENGL);    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClearDepth(1.0f);
    glDepthFunc(GL_LESS);  
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, static_cast<double>(window_width)/window_height,
                   0.01, 1000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    GLfloat light_ambient[] = {0.05f, 0.05f, 0.05f, 1.0f};
    GLfloat light_diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
    GLfloat light_specular[] = {0.1f, 0.1f, 0.1f, 1.0f};
    GLfloat light_position[] = {0.01f, 0.01f, 0.01f, 1.0f};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT1, GL_POSITION, light_position);
    //glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, &light_model_two_side);
    glEnable(GL_LIGHT1);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    c.set_position(camera_x_pos, camera_y_pos, camera_z_pos);
    c.set_forward(0.0, 1.0, 0.0);
    c.set_up(0.0, 0.0, 1.0);
    c.set_along(1.0, 0.0, 0.0);
}

void
DisplayKeyDown(SDL_KeyboardEvent *key)
{
    static double sens = 1;
    double v0, v1, v2;
    switch(key->keysym.sym)
    {
    case 'w':
        walk = sens;
        break;
    case 's':
        walk = -sens;
        break;
    case 'a':
        strafe = sens;
        break;
    case 'd':
        strafe = -sens;
        break;
    case 'c':
        roll = sens/50;
        break;
    case 'v':
        roll = -sens/50;
        break;
    case 'f':
        yaw = sens/50;
        break;
    case 'g':
        yaw = -sens/50;
        break;
    case 'e':
        pitch = sens/50;
        break;
    case 'r':
        pitch = -sens/50;
        break;
    case 'l':
        enable_lighting = !enable_lighting;
        if(enable_lighting)
        {
            glEnable(GL_LIGHTING);
            std::cout << "Lighting enabled" << std::endl;
        }
        else
        {
            glDisable(GL_LIGHTING);
            std::cout << "Lighting disabled" << std::endl;
        }
        break;
    case '+':
        sens *= 2;
        std::cout << "Sensitivity: " << sens << std::endl;
        break;
    case '-':
        sens *= 0.5;
        std::cout << "Sensitivity: " << sens << std::endl;
        break;
    case 'n':
        wireframe = !wireframe;
        if(wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        break;
    case 'h':
        if(draw_outer+2 == segmentor->surface_count()) draw_outer = 1;
        else if(draw_outer+2 > segmentor->surface_count()) draw_outer = 0;
        else draw_outer = (draw_outer+2)%segmentor->surface_count();
        std::cout << "Drawing surface: " << draw_outer << std::endl;
        break;
    case 'k':
        draw_balls = !draw_balls;
        if(draw_balls) std::cout << "Drawing flow line balls" << std::endl;
        else std::cout << "Not drawing flow line balls" << std::endl;
        break;
    case 'j':
        draw_flow_lines = !draw_flow_lines;
        if(draw_flow_lines) std::cout << "Drawing flow lines" << std::endl;
        else std::cout << "Not drawing flow lines" << std::endl;
        break;
    case 'b':
        segmentor->step(*image_tracer, front_edge_links, maximum_front_size);
        break;
    case ',':
        segmentor->complete_step(*image_tracer, front_edge_links,
            maximum_front_size);
        break;
    case '9':
        infinite_loop = !infinite_loop;
        if(infinite_loop)
            std::cout << "Mouse off and iterating infinitely!" << std::endl;
        else std::cout << "Mouse on and stopped iterating!" << std::endl;
        break;
    case 27:
        exit(0);
        break;
    }
}

void
DisplayKeyUp(SDL_KeyboardEvent *key)
{
    switch(key->keysym.sym)
    {
    case 'w':
    case 's':
        walk = 0;
        break;
    case 'a':
    case 'd':
        strafe = 0;
        break;
    case 'c':
    case 'v':
        roll = 0;
        break;
    case 'f':
    case 'g':
        yaw = 0;
        break;
    case 'e':
    case 'r':
        pitch = 0;
        break;
    }
}

void
main_loop(opfront *_segmentor,
    image_tracer_type *_image_tracer,
    GtsSurface *_surface,
    const std::string& output_prefix,
    int _front_edge_links,
    int _maximum_front_size)
{
    surface = _surface;
    segmentor = _segmentor;
    front_edge_links = _front_edge_links;
    maximum_front_size = _maximum_front_size;
    image_tracer = _image_tracer;
     // Compute center of mass for camera position.
    GtsVector cm;
    gts_surface_center_of_area(surface, cm);
    std::cout << "Segmentation center of mass: " << cm[0] << " " << cm[1]
              << " " << cm[2] << std::endl;
    camera_x_pos -= cm[0];
    camera_y_pos -= cm[1];
    camera_z_pos -= cm[2];

    init_gl();
    SDL_Event event;
    int running = 1;

    int cur_time = SDL_GetTicks(), last_time = 0, k = 0, delta = 0;
    while(running)
    {
        if(!(++k%100))
        {
            delta = cur_time-last_time;
            last_time = cur_time;
            k = 0;
        }
        cur_time = SDL_GetTicks();
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_KEYDOWN:
                DisplayKeyDown(&event.key);
                break;
            case SDL_KEYUP:
                DisplayKeyUp(&event.key);
                break;
            case SDL_MOUSEBUTTONDOWN:
                std::cout << "Mouse look on!" << std::endl;
                SDL_ShowCursor(SDL_DISABLE);
                mouse_look = true;
                last_x = window_width/2;
                last_y = window_height/2;
                SDL_WarpMouse(window_width/2, window_height/2);
                break;
            case SDL_MOUSEBUTTONUP:
                std::cout << "Mouse look off!" << std::endl;
                SDL_ShowCursor(SDL_ENABLE);
                mouse_look = false;
                break;
            case SDL_MOUSEMOTION:
                if(!infinite_loop && mouse_look)
                    mouse_motion(event.motion.x, event.motion.y);
                break;
            case SDL_QUIT:
                running = 0;
                break;
            }
        }
        if(delta == 0) delta = -1;
        std::cout << cur_time << " FPS: " << 100000/delta
                  << "                  \r" << std::flush;
        c.walk(walk);
        c.strafe(strafe);
        c.roll(roll);
        c.yaw(yaw);
        c.pitch(pitch);
        
        int snap = 40;
        if(mouse_look &&
           (last_x < snap || last_x > window_width-snap || last_y < snap ||
            last_y > window_height-snap))
        {
            last_x = window_width/2;
            last_y = window_height/2;
            SDL_WarpMouse(window_width/2, window_height/2);
        }
        if(infinite_loop)
        {
            bool backup = draw_flow_lines;
            draw_flow_lines = false;
            update();
            draw_flow_lines = backup;
            cimg_library::CImg<unsigned char> screen_dump(window_width,
                window_height, 1, 3);
            unsigned char data[window_width*window_height*3];
            glReadPixels(0, 0, window_width, window_height, GL_RGB,
                         GL_UNSIGNED_BYTE, data);
            cimg_forXYZC(screen_dump, x, y, z, c)
            {
                screen_dump(x, y, z, c) = data[(y*window_width+x)*3+c];
            }
            if(output_prefix != "")
            {
                std::stringstream ss;
                ss << "segment_iteration_" << loop_count++ << ".bmp";
                screen_dump.save(ss.str().c_str());
            }
            segmentor->complete_step(*image_tracer, front_edge_links,
                maximum_front_size);
        }
        else update();
    }
}

