/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __SUBDIVISION_H__
#define __SUBDIVISION_H__

#include <gts.h>
#include <math.h>
#include "util/gts_repair.h"
#include "opfront/multi_surface.h"

namespace subdivision
{
    GtsVertex*
    //multi_segment_midvertex(GtsEdge *e, GtsVertexClass *klass, gpointer data)
    multi_segment_midvertex(GtsEdge *e, GtsVertexClass *klass)
    {
        GtsMVertex *v1 = GTS_MVERTEX(e->segment.v1),
                   *v2 = GTS_MVERTEX(e->segment.v2);
        return GTS_VERTEX(mvertex_midvertex2(v1, v2));
    }
    

    gdouble
    refine_cost(gpointer item, gpointer data)
    {
        GtsSegment *s = GTS_SEGMENT(item);
        GtsMVertex *v1 = GTS_MVERTEX(s->v1), *v2 = GTS_MVERTEX(s->v2);
        if(v1->moving && v2->moving)
            return -gts_point_distance2(GTS_POINT(s->v1), GTS_POINT(s->v2));
        else return std::numeric_limits<gdouble>::max();
    }

    gboolean
    refine_length_stop(gdouble cost, guint length, gpointer data)
    {
        /*std::cout << "Refine: " << cost << " " << length << " "
                  << *static_cast<gdouble*>(data) << std::endl;*/
        if(fabs(cost) < *static_cast<gdouble*>(data)) return true;
        return false;
    }
 
    gboolean
    coarsen_length_stop(gdouble cost, guint length, gpointer data)
    {
        //std::cout << "Coarsen: " << cost << " " << length << " "
        //          << *static_cast<gdouble*>(data) << std::endl;
        if(cost > *static_cast<gdouble*>(data)) return true;
        else return false;
    } 

    void
    stats(GtsSurface *s)
    {
        GtsSurfaceQualityStats stat;
        gts_surface_quality_stats(s, &stat);
        std::cout << "Edges: " << stat.edge_length.n << " ["
                  << stat.edge_length.min << ", " << stat.edge_length.max
                  << "] " << stat.edge_length.mean << std::endl;
    }

    void
    balance_length(GtsSurface *s, gdouble maximum_edge_length)
    {
        if(maximum_edge_length <= 0) return;
        gdouble refine_length = maximum_edge_length*maximum_edge_length,
            coarsen_length = maximum_edge_length*maximum_edge_length/4,
            fold = 3.14/180;
        coarsen_length *= coarsen_length;
#ifdef OPFRONT_DEBUG
        boost::progress_timer timer;
        stats(s);
#endif
        // Coarsen too fine edges.
        gts_surface_coarsen_front(s, NULL, NULL,
            (GtsCoarsenFunc)multi_segment_midvertex,
            NULL, coarsen_length_stop, &coarsen_length, fold);
        // Repair using a combination of smoothing and coarsening.
        gts_repair(s, 10, multi_segment_midvertex);
        // Refine too coarse edges.
        gts_surface_refine_front(s, NULL, NULL,
            (GtsRefineFunc)multi_segment_midvertex,
            NULL, refine_length_stop, &refine_length);
#ifdef OPFRONT_DEBUG
        stats(s);
#endif
    }
}

#endif

