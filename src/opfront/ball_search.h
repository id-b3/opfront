/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __BALL_SEARCH_H__
#define __BALL_SEARCH_H__

static const double
    ball_search_min_angle = 0.001,
    ball_search_min_radius_interval = 0.001;

template<class FLOAT_TYPE>
void
ball_search(
    int theta_angles,
    int phi_angles,
    int radius_intervals,
    FLOAT_TYPE (*potential)(FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, void*),
    void *params,
    FLOAT_TYPE x,
    FLOAT_TYPE y,
    FLOAT_TYPE z,
    FLOAT_TYPE &radius,
    FLOAT_TYPE radius_interval,
    FLOAT_TYPE &theta,
    FLOAT_TYPE theta_angle,
    FLOAT_TYPE &phi,
    FLOAT_TYPE phi_angle,
    FLOAT_TYPE &max_potential)
{
    FLOAT_TYPE next_theta = theta, next_phi = phi, next_radius = radius;
    if(theta_angle < ball_search_min_angle ||
       phi_angle < ball_search_min_angle ||
       radius_interval < ball_search_min_radius_interval) return;
    //std::cout << "M=[" << std::endl;
    for(int i = 0; i < theta_angles; i++)
    {
        FLOAT_TYPE t = theta-theta_angle/2+(i+0.5)*theta_angle/theta_angles;
        for(int j = 0; j < phi_angles; j++)
        {
            FLOAT_TYPE p = phi-phi_angle/2+(j+0.5)*phi_angle/phi_angles;
            for(int k = 0; k < radius_intervals; k++)
            {
                FLOAT_TYPE r = radius-radius_interval/2+(k+0.5)*
                    radius_interval/radius_intervals;
                FLOAT_TYPE c = potential(x+r*cos(t)*sin(p),
                    y+r*sin(t)*sin(p), z+r*cos(p), params);
                //std::cout << r*cos(t)*sin(p) << ","
                //          << r*sin(t)*sin(p) << ","
                //          << r*cos(p) << ","
                //          << c << ";" << std::endl;
                if(c > max_potential)
                {
                    next_theta = t;
                    next_phi = p;
                    next_radius = r;
                    max_potential = c;
                }
            }
        }
    }
    //std::cout << "]" << std::endl;
    theta = next_theta;
    phi = next_phi;
    radius = next_radius;
    ball_search(2, 2, 2, potential, params, x, y, z, radius,
        radius_interval/radius_intervals, theta, theta_angle/theta_angles,
        phi, phi_angle/phi_angles,
        max_potential);
}

template<class FLOAT_TYPE>
bool
ball_search(
    FLOAT_TYPE (*potential)(FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, void*),
    void *params,
    FLOAT_TYPE &x,
    FLOAT_TYPE &y,
    FLOAT_TYPE &z,
    FLOAT_TYPE radius,
    FLOAT_TYPE &max_potential)
{
    static const FLOAT_TYPE pi = 3.141592653589793238462643383279502884;
    FLOAT_TYPE theta = pi, phi = pi/2,
        p = -std::numeric_limits<FLOAT_TYPE>::max(), sradius = radius/2;
    ball_search(4, 4, 2, potential, params, x, y, z, sradius, radius, theta,
        2*pi, phi, pi, p);
    // Only continue if larger potential found and if this potential is on
    // the surface of the ball.
    if(p > max_potential &&
       fabs(sradius-radius) < ball_search_min_radius_interval*10)
    {
        x += radius*cos(theta)*sin(phi);
        y += radius*sin(theta)*sin(phi);
        z += radius*cos(phi);
        max_potential = p;
        return true;
    } else return false;
}

template<class FLOAT_TYPE>
void
ball_search_trace(
    FLOAT_TYPE (*potential)(FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, void*),
    void *params,
    FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z,
    FLOAT_TYPE radius,
    int iterations,
    std::vector<FLOAT_TYPE> &flow_line)
{
    FLOAT_TYPE p = potential(x, y, z, params),
        wradius = radius/2;
    while(iterations--)
    {
        if(ball_search(potential, params, x, y, z, wradius, p))
        {
            flow_line.push_back(x);
            flow_line.push_back(y);
            flow_line.push_back(z);
        } else break;
        wradius = radius;
    }
}

template<class FLOAT_TYPE>
void
trace_batch(
    const std::vector<FLOAT_TYPE> &points,
    FLOAT_TYPE interval,
    FLOAT_TYPE (*potential)(FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, void*),
    void *params,
    double max_length,
    std::vector<std::vector<FLOAT_TYPE> > &flow_lines)
{
    int max_it = max_length <= 0? std::numeric_limits<int>::max():
        static_cast<int>(max_length/interval);
    for(int i = 0; i < points.size(); i+=3)
    {
        ball_search_trace(potential, params, points[i],
            points[i+1], points[i+2], interval, max_it,
            flow_lines[i/3]);
    }
}

#endif

