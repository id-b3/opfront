/* Modified GTS library code
 * Copyright (C) 2014 Jens Petersen
 *
 * GTS - Library for the manipulation of triangulated surfaces
 * Copyright (C) 1999 St�phane Popinet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gts.h>
#include "opfront/gts_plugin.h"

#define HEAP_INSERT_EDGE(h, e) (GTS_OBJECT(e)->reserved =\
    gts_eheap_insert(h, e))
#define HEAP_REMOVE_EDGE(h, e) (gts_eheap_remove(h,\
    (GtsEHeapPair*)(GTS_OBJECT(e)->reserved)), GTS_OBJECT(e)->reserved = NULL)

static void
heap_insert_front(GtsEHeap *heap, GtsEdge *e)
{
    GtsMVertex *m0 = GTS_MVERTEX(e->segment.v1),
               *m1 = GTS_MVERTEX(e->segment.v2);
    if(m0->moving && m1->moving)
    {
        gts_eheap_insert(heap, e);
    }
}

static void
midvertex_insertion_front(GtsEdge *e,
    GtsSurface *surface,
    GtsEHeap *heap,
    GtsRefineFunc refine_func,
    gpointer refine_data,
    GtsVertexClass *vertex_class,
    GtsEdgeClass *edge_class)
{
    GtsVertex *midvertex;
    GtsEdge *e1, *e2;
    GSList *i;

    midvertex = (*refine_func)(e, vertex_class, refine_data);
    e1 = gts_edge_new(edge_class, GTS_SEGMENT (e)->v1, midvertex);
    heap_insert_front(heap, e1);
    e2 = gts_edge_new(edge_class, GTS_SEGMENT (e)->v2, midvertex);
    heap_insert_front(heap, e2);

    /* creates new faces and modifies old ones */
    i = e->triangles;
    while (i)
    {
        GtsTriangle *t = GTS_TRIANGLE(i->data);
        GtsVertex *v1, *v2, *v3;
        GtsEdge *te2, *te3, *ne, *tmp;

        gts_triangle_vertices_edges(t, e, &v1, &v2, &v3, &e, &te2, &te3);
        ne = gts_edge_new (edge_class, midvertex, v3);
        heap_insert_front(heap, ne);
        if(GTS_SEGMENT(e1)->v1 == v2)
        {
            tmp = e1; e1 = e2; e2 = tmp;
        }
        e1->triangles = g_slist_prepend(e1->triangles, t);
        ne->triangles = g_slist_prepend(ne->triangles, t);
        te2->triangles = g_slist_remove(te2->triangles, t);
        t->e1 = e1; t->e2 = ne; t->e3 = te3;
        GtsFace *f = gts_face_new(surface->face_class, e2, te2, ne);
        gts_surface_add_face(surface, f); 
        i = i->next;
    }
    /* destroys edge */
    g_slist_free(e->triangles);
    e->triangles = NULL;
    gts_object_destroy(GTS_OBJECT(e));
}

static gdouble
edge_length2_inverse(GtsSegment *s)
{
  return -gts_point_distance2(GTS_POINT(s->v1), GTS_POINT(s->v2));
}

static void
create_heap_refine_front(GtsEdge *e, GtsEHeap *heap)
{
    heap_insert_front(heap, e);
}
  
/**
 * gts_surface_refine:
 * @surface: a #GtsSurface.
 * @cost_func: a function returning the cost for a given edge.
 * @cost_data: user data to be passed to @cost_func.
 * @refine_func: a #GtsRefineFunc.
 * @refine_data: user data to be passed to @refine_func.
 * @stop_func: a #GtsStopFunc.
 * @stop_data: user data to be passed to @stop_func.
 *
 * Refine @surface using a midvertex insertion technique. All the
 * edges of @surface are ordered according to @cost_func. The edges
 * are then processed in order until @stop_func returns %TRUE. Each
 * edge is split in two and new edges and faces are created.
 *
 * If @cost_func is set to %NULL, the edges are sorted according 
 * to their length squared (the longest is on top).
 *
 * If @refine_func is set to %NULL gts_segment_midvertex() is used.
 * 
 */
void
gts_surface_refine_front(GtsSurface *surface,
    GtsKeyFunc cost_func,
    gpointer cost_data,
    GtsRefineFunc refine_func,
    gpointer refine_data,
    GtsStopFunc stop_func,
    gpointer stop_data)
{
    GtsEHeap *heap;
    GtsEdge *e;
    gdouble top_cost;

    g_return_if_fail(surface != NULL);
    g_return_if_fail(stop_func != NULL);

    if(cost_func == NULL) cost_func = (GtsKeyFunc)edge_length2_inverse;
    if(refine_func == NULL)
        refine_func = (GtsRefineFunc) gts_segment_midvertex;

    heap = gts_eheap_new(cost_func, cost_data);
    gts_eheap_freeze(heap);
    gts_surface_foreach_edge(surface, (GtsFunc)create_heap_refine_front, heap);
    gts_eheap_thaw (heap);
    while((e = GTS_EDGE(gts_eheap_remove_top(heap, &top_cost))) &&
           !(*stop_func)(top_cost, gts_eheap_size(heap)+
           gts_edge_face_number(e, surface)+2,stop_data))
    {
        midvertex_insertion_front(e, surface, heap, refine_func, refine_data,
            surface->vertex_class, surface->edge_class);
    }
    gts_eheap_destroy(heap);
}

static GtsVertex*
edge_collapse_front(GtsEdge *e,
    GtsEHeap *heap,
    GtsCoarsenFunc coarsen_func,
    gpointer coarsen_data,
    GtsVertexClass *klass,
    gdouble maxcosine2)
{
    GSList *i;
    GtsVertex *v1 = GTS_SEGMENT(e)->v1, *v2 = GTS_SEGMENT(e)->v2, *mid;

    /* if the edge is degenerate (i.e. v1 == v2), destroy and return */
    if(v1 == v2)
    {
        gts_object_destroy(GTS_OBJECT(e));
        return NULL;
    }

    if(!gts_edge_collapse_is_valid(e))
    {
        GTS_OBJECT(e)->reserved = 
            gts_eheap_insert_with_key(heap, e, G_MAXDOUBLE);
        return NULL;
    }

    mid = (*coarsen_func)(e, klass, coarsen_data);

    if(gts_edge_collapse_creates_fold(e, mid, maxcosine2))
    {
        GTS_OBJECT (e)->reserved = 
            gts_eheap_insert_with_key(heap, e, G_MAXDOUBLE);
        gts_object_destroy (GTS_OBJECT (mid));
        return NULL;
    }

    gts_object_destroy(GTS_OBJECT(e));

    gts_vertex_replace(v1, mid);
    gts_object_destroy(GTS_OBJECT(v1));
    gts_vertex_replace(v2, mid);
    gts_object_destroy(GTS_OBJECT(v2));

    /* destroy duplicate edges */
    i = mid->segments;
    while (i)
    {
        GtsEdge *e1 = GTS_EDGE(i->data);
        GtsEdge *duplicate;
        while((duplicate = gts_edge_is_duplicate (e1)))
        {
            gts_edge_replace(duplicate, GTS_EDGE (e1));
            if(GTS_OBJECT(duplicate)->reserved)
            {
                HEAP_REMOVE_EDGE(heap, duplicate);
            }
            gts_object_destroy(GTS_OBJECT(duplicate));
        }
        i = i->next;
        if(!e1->triangles)
        {
        /* e1 is the result of the collapse of one edge of a pair of identical
         faces (it should not happen unless duplicate triangles are present in
         the initial surface) */
            //g_warning ("file %s: line %d (%s): probably duplicate triangle.", __FILE__, __LINE__, G_GNUC_PRETTY_FUNCTION);
            g_warning ("file %s: line %d (%s): probably duplicate triangle.", __FILE__, __LINE__, G_STRFUNC);
            if(GTS_OBJECT(e1)->reserved)
            {
                HEAP_REMOVE_EDGE(heap, e1);
            }
            gts_object_destroy(GTS_OBJECT(e1));
            if(i == NULL) /* mid has been destroyed */
                mid = NULL;
        }
    }
    return mid;
}

static void
update_closest_neighbors_front(GtsVertex *v, GtsEHeap *heap)
{
    GSList *i = v->segments;
    while(i)
    {
        GtsSegment *s = GTS_SEGMENT(i->data);
        if(GTS_IS_EDGE(s))
        {
            if(GTS_OBJECT(s)->reserved)
            {
                HEAP_REMOVE_EDGE(heap, GTS_EDGE(s));
            }
            HEAP_INSERT_EDGE(heap, GTS_EDGE(s));
        }
        i = i->next;
    }
}

static void
update_2nd_closest_neighbors_front(GtsVertex *v, GtsEHeap *heap)
{
    GSList *i = v->segments;
    GSList *list = NULL;

    while(i)
    {
        GtsSegment *s = GTS_SEGMENT(i->data);
        if(GTS_IS_EDGE(s))
        {
            GtsVertex *v1 = s->v1 == v? s->v2: s->v1;
            GSList *j = v1->segments;
            while(j)
            {
                GtsSegment *s1 = GTS_SEGMENT(j->data);
                if(GTS_IS_EDGE(s1) && !g_slist_find(list, s1))
                    list = g_slist_prepend(list, s1);
                j = j->next;
            }
        }
        i = i->next;
    }
    i = list;
    while(i)
    {
        GtsEdge *e = GTS_EDGE(i->data);
        if(GTS_OBJECT(e)->reserved)
        {
            HEAP_REMOVE_EDGE (heap, e);
        }
        HEAP_INSERT_EDGE(heap, e);
        i = i->next;
    }
    g_slist_free(list);
}

static gdouble
edge_length2(GtsEdge * e)
{
    return gts_point_distance2(GTS_POINT(GTS_SEGMENT(e)->v1), 
        GTS_POINT(GTS_SEGMENT (e)->v2));
}

static void
create_heap_coarsen_front(GtsEdge *e, GtsEHeap *heap)
{
    GtsMVertex *m0 = GTS_MVERTEX(e->segment.v1),
               *m1 = GTS_MVERTEX(e->segment.v2);
    if(!m0->moving && !m1->moving)
    {
        GTS_OBJECT(e)->reserved = NULL;
        return;
    }
    HEAP_INSERT_EDGE(heap, e);
}

/**
 * gts_surface_coarsen:
 * @surface: a #GtsSurface.
 * @cost_func: a function returning the cost for a given edge.
 * @cost_data: user data to be passed to @cost_func.
 * @coarsen_func: a #GtsCoarsenVertexFunc.
 * @coarsen_data: user data to be passed to @coarsen_func.
 * @stop_func: a #GtsStopFunc.
 * @stop_data: user data to be passed to @stop_func.
 * @minangle: minimum angle between two neighboring triangles.
 *
 * The edges of @surface are sorted according to @cost_func to 
 * create a priority heap (a #GtsEHeap). The edges are extracted in
 * turn from the top of the heap and collapsed (i.e. the vertices are
 * replaced by the vertex returned by the @coarsen_func function)
 * until the @stop_func functions returns %TRUE.
 *
 * If @cost_func is set to %NULL, the edges are sorted according 
 * to their length squared (the shortest is on top).
 *
 * If @coarsen_func is set to %NULL gts_segment_midvertex() is used.
 *
 * The minimum angle is used to avoid introducing faces which would be folded.
 */
void
gts_surface_coarsen_front(GtsSurface *surface,
    GtsKeyFunc cost_func,
    gpointer cost_data,
    GtsCoarsenFunc coarsen_func,
    gpointer coarsen_data,
    GtsStopFunc stop_func,
    gpointer stop_data,
    gdouble minangle)
{
    GtsEHeap *heap;
    GtsEdge *e;
    gdouble top_cost;
    gdouble maxcosine2;

    g_return_if_fail (surface != NULL);
    g_return_if_fail (stop_func != NULL);

    if(cost_func == NULL)
        cost_func = (GtsKeyFunc)edge_length2;
    if(coarsen_func == NULL)
        coarsen_func = (GtsCoarsenFunc)gts_segment_midvertex;

    heap = gts_eheap_new(cost_func, cost_data);
    maxcosine2 = cos(minangle); maxcosine2 *= maxcosine2;

    gts_eheap_freeze(heap);
    gts_surface_foreach_edge(surface, (GtsFunc)create_heap_coarsen_front,
        heap);
    gts_eheap_thaw(heap);
    /* we want to control edge destruction manually */
    gts_allow_floating_edges = TRUE;
    while((e = GTS_EDGE(gts_eheap_remove_top(heap, &top_cost))) &&
          (top_cost < G_MAXDOUBLE) &&
          !(*stop_func)(top_cost, gts_eheap_size(heap)- 
          gts_edge_face_number(e, surface), stop_data))
    {
        GtsVertex *v = edge_collapse_front(e, heap, coarsen_func, coarsen_data,
            surface->vertex_class, maxcosine2);
        if(v != NULL) update_2nd_closest_neighbors_front(v, heap);
    }
    gts_allow_floating_edges = FALSE;

    /* set reserved field of remaining edges back to NULL */
    if(e) GTS_OBJECT(e)->reserved = NULL;
    gts_eheap_foreach(heap, (GFunc)gts_object_reset_reserved, NULL);

    gts_eheap_destroy(heap);
}
