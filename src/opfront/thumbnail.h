/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __THUMBNAIL_H__
#define __THUMBNAIL_H__

struct thumbnail_data
{
    cimg_library::CImg<unsigned char> image;
    int axis;
};

gint
generate_thumbnail(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    thumbnail_data *d = static_cast<thumbnail_data*>(data);
    gdouble p[3];
    multi_surface_position(m, 0, p);
    d->image.xyz2ijk(p[0], p[1], p[2]);
    switch(d->axis)
    {
    case 'x':
        p[0] = p[1];
        p[1] = p[2];
        break;
    case 'y':
        p[1] = p[2];
        break;
    }
    int xi = static_cast<int>(p[0]+0.5),
        yi = static_cast<int>(p[1]+0.5),
        zi = static_cast<int>(p[2]+0.5);
    if(xi >= 0 && xi < d->image.width() &&
       yi >= 0 && yi < d->image.height())
    {
        if(m->moving) d->image(xi, yi, 0) = 255;
        else d->image(xi, yi, 2) = 255;
    }
    return 0;
}

template<class CIMG_TYPE>
void
create_thumbnail(char thumbnail_axis, const char* filename,
    const cimg_library::CImg<CIMG_TYPE> &volume, GtsSurface *surface)
{
    int d0 = 0, d1 = 0;
    switch(thumbnail_axis)
    {
    case 'x':
        d0 = volume.height();
        d1 = volume.depth();
        break;
    case 'y':
        d0 = volume.width();
        d1 = volume.depth();
        break;
    case 'z':
        d0 = volume.width();
        d1 = volume.height();
        break;
    default:
        return;
    }
    assert(d0 > 0);
    assert(d1 > 0);
    thumbnail_data d;
    d.image.assign(d0, d1, 1, 3, 0);
    d.axis = thumbnail_axis;
    d.image.set_metadata(volume);
    gts_surface_foreach_vertex(surface, generate_thumbnail, (gpointer)(&d));
    d.image.save(filename);
}

#endif

