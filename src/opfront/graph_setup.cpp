/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include "opfront/graph_setup.h"
#include "util/statistics.h"

static const int convergence_limit = 2;

static void
graph_setup_edge_constraint(multi_graph<ofloat>& graph,
    GtsMVertex **vertices,
    int vertex_count,
    int (*edge_offset)(int, int, int, int, void*),
    void *edge_offset_params,
    void (*edge_constraint)(int, int, int, int, int&, int&, void*),
    void *edge_constraint_params,
    int surface_i,
    int surface_j,
    int column_i,
    int column_j)
{
    int minus_delta, plus_delta, offset = edge_offset(surface_i, surface_j,
        column_i, column_j, edge_offset_params);
    //if(offset < 0) return;
    offset += multi_surface_index(vertices[column_j], 0)-
        multi_surface_index(vertices[column_i], 0);
    graph.set_neighbors(surface_i, column_i, surface_j, column_j, offset);
    edge_constraint(surface_i, surface_j, column_i, column_j, minus_delta,
        plus_delta, edge_constraint_params);
    graph.add_edge_range(surface_i, column_i, surface_j, column_j,
        minus_delta, plus_delta);
}

void
graph_setup(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    GtsEdge **edges,
    int edge_count,
    multi_graph<ofloat>& graph,
    int (*edge_offset)(int, int, int, int, void*),
    void *edge_offset_params,
    void (*edge_constraint)(int, int, int, int, int&, int&, void*),
    void *edge_constraint_params,
    ofloat (*surface_cost)(int, int, int, void*),
    void *surface_cost_params)
{
    // Vertex weights.
#ifdef OPFRONT_DEBUG
    std::vector<ofloat> weights;
#endif
    for(int i = 0; i < vertex_count; i++)
    {
        int length = mvertex_column_size(vertices[i]);
        for(int j = 0; j < surface_count; j++)
        {
            graph.set_column_length(j, i, length);
            for(int k = 0; k < length; k++)
            {
                ofloat w = surface_cost(j, i, k, surface_cost_params);
#ifdef OPFRONT_DEBUG
                weights.push_back(w);
#endif
                graph.add_node(j, i, k, w);
            }
        }
    }
#ifdef OPFRONT_DEBUG
    std::cout << "Vertex surface cost " << statistics::summary(weights)
              << std::endl;
#endif
    // Surface separation constraints.
    //  Note this is O(n^2) in the number of surfaces, could be fixed with
    //  some kind of surface interaction registration thingy...
    for(int i = 0; i < vertex_count; i++)
    {
        for(int j = 0; j < surface_count-1; j++)
        {
            for(int k = j+1; k < surface_count; k++)
            {
                graph_setup_edge_constraint(graph, vertices, vertex_count,
                    edge_offset, edge_offset_params, edge_constraint,
                    edge_constraint_params, j, k, i, i);
            }
        }
    }
    // Smoothness constraints.
    for(int i = 0; i < edge_count; i++)
    {
        const GtsEdge* e = edges[i];
        const GtsMVertex *v0 = GTS_MVERTEX(e->segment.v1),
                         *v1 = GTS_MVERTEX(e->segment.v2);
        int column_i = v0->index, column_j = v1->index;
        for(int j = 0; j < surface_count; j++)
        {
            graph_setup_edge_constraint(graph, vertices, vertex_count,
                edge_offset, edge_offset_params, edge_constraint,
                edge_constraint_params, j, j, column_i, column_j);
        }
    }
    graph.construct();
}

void
graph_update_surface(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    multi_graph<ofloat> &graph,
    bool (*mask_vertex)(GtsMVertex*, void*),
    void *mask_vertex_params)
{
    for(int j = 0; j < vertex_count; j++)
    {
        GtsMVertex* v = vertices[j];
        if(mask_vertex && !mask_vertex(v, mask_vertex_params)) continue;
        int length = mvertex_column_size(v), k;
        for(int i = 0; i < surface_count; i++)
        {
            for(k = 0; k < length; k++) if(!graph.source(i, j, k)) break;
            k--;
            assert(k >= 0);
            multi_surface_index(v, i, k);
        }
    }
}

void
graph_update_surface_with_min_marginals(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    multi_graph<ofloat> &graph,
    int min_marginal_type,
    bool (*mask_vertex)(GtsMVertex*, void*),
    void *mask_vertex_params)
{
    for(int j = 0; j < vertex_count; j++)
    {
        GtsMVertex* v = vertices[j];
        if(mask_vertex && !mask_vertex(v, mask_vertex_params)) continue;
        int length = mvertex_column_size(v), s;
        assert(length > 0);
        std::vector<ofloat> min_marginal_s(length),
            min_marginal_t(length),
            min_marginals(2*surface_count*length);
        for(int i = 0; i < surface_count; i++)
        {
            switch(min_marginal_type)
            {
            // 0/1 fake marginals.
            case 1:
                for(int k = 0; k < length; k++)
                {
                    if(graph.source(i, j, k))
                    {
                        min_marginal_s[k] = 0;
                        min_marginal_t[k] = 1;
                        s = k;
                    }
                    else
                    {
                        min_marginal_s[k] = 1;
                        min_marginal_t[k] = 0;
                    }
                }
                break;
            // Banded binary min marginal computation.
            case 2:
                for(s = 0; s < length && graph.source(i, j, s); s++);
                s--;
                ofloat mm_s, mm_t;
                graph.source(i, j, s, mm_s, mm_t);
                for(int k = 0; k <= s; k++)
                {
                    min_marginal_s[k] = mm_s;
                    min_marginal_t[k] = mm_t;
                }
                if(s < length-1)
                {
                    graph.source(i, j, s+1, mm_s, mm_t);
                    for(int k = s+1; k < length; k++)
                    {
                        min_marginal_s[k] = mm_s;
                        min_marginal_t[k] = mm_t;
                    }
                }
                break;
            // Full binary min marginal computation.
            case 3:
                s = graph.source(i, j, &min_marginal_s[0], &min_marginal_t[0]);
                break;
            // Banded n-ary min marginal computation
            case 4:
                for(s = 0; s < length && graph.source(i, j, s); s++);
                s--;
                min_marginal_s[s] = graph.get_maxflow();
                if(s)
                {
                    graph.surface(i, j, s-1, min_marginal_s[s-1]);
                    for(int k = 0; k < s-1; k++)
                        min_marginal_s[k] = std::numeric_limits<ofloat>::max();
                    assert(min_marginal_s[s] <= min_marginal_s[s-1]+0.001);
                }
                if(s < length-1)
                {
                    graph.surface(i, j, s+1, min_marginal_s[s+1]);
                    for(int k = s+2; k < length; k++)
                        min_marginal_s[k] = std::numeric_limits<ofloat>::max();
                    assert(min_marginal_s[s] <= min_marginal_s[s+1]+0.001);
                }
                break;
            // Full n-ary min marginal computation
            default:
                for(s = 0; s < length && graph.source(i, j, s); s++);
                s--;
                for(int k = 0; k < length; k++)
                    graph.surface(i, j, k, min_marginal_s[k]);
                break;
            }
            // Store it in this way as well to be compatible with how
            // surface position is computed for types 1, 2, and 3.
            // Note: should probably think about how to clean this up!
            if(min_marginal_type > 3)
                for(int k = 1; k < length; k++)
                    min_marginal_t[k] = min_marginal_s[k-1];
            min_marginal_t[0] = std::numeric_limits<ofloat>::max();

            //for(int k = 1; k < length; k++)
            //{
            //    certainty[k*surface_count+i] =
            //        1.0/(1+exp((min_marginal_s[k]-min_marginal_t[k])/
            //        certainty_scale[i]));
            //}
            multi_surface_index(v, i, s);
            for(int k = 0; k < length; k++)
            {
                min_marginals[2*(k*surface_count+i)] = min_marginal_t[k]; 
                min_marginals[2*(k*surface_count+i)+1] = min_marginal_s[k]; 
            }
        }
        mvertex_min_marginals(v, &min_marginals[0]);
    }
}

