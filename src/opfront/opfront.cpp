/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include "opfront/opfront.h"
#include "opfront/graph_setup.h"
#include "opfront/subdivision.h"
#include "util/statistics.h"
#include "util/lalg.h"
#ifdef OPFRONT_PRINT_CUT_TIME
#include <boost/chrono/thread_clock.hpp>
#endif


// Variable used to stop oscilleratory behaviour (should perhaps be
// user accessible option).
static const ofloat movement_distance = 0.05;

static bool
mask_vertex(GtsMVertex *m, void* params)
{
    return true;
}

void
line_stat(const vertex_container &vertices)
{
    std::vector<ofloat> inner_intervals, outer_intervals;
    std::vector<ofloat> inner_length, outer_length, length;
    for(int i = 0; i < vertices.size(); i++)
    {
        const GtsMVertex *m = vertices[i];
        gint l = mvertex_column_size(m),
            midpoint = multi_surface_index(m, 0);
        //const coordinate_vector &line = lines[i];
        ofloat ilength = 0, olength = 0;
        for(int j = 0; j < l-1; j++)
        {
            const gdouble *p = mvertex_position(m, j),
                *n = mvertex_position(m, j+1);
            ofloat d = linalg_dis_ve3(p, n);
            if(j <= midpoint)
            {
                inner_intervals.push_back(d);
                ilength += d;
            }
            else
            {
                outer_intervals.push_back(d);
                olength += d;
            }
        }
        inner_length.push_back(ilength);
        outer_length.push_back(olength);
        length.push_back(ilength+olength);
    }
    std::cout << "Length " << statistics::summary(length) << std::endl;
    std::cout << "Inner length " << statistics::summary(inner_length)
              << std::endl;
    std::cout << "Outer length " << statistics::summary(outer_length)
              << std::endl;
    if(length.size() > 0)
    {
        std::cout << "Inner interval length "
                  << statistics::summary(inner_intervals) << std::endl;
        std::cout << "outer interval length "
                  << statistics::summary(outer_intervals) << std::endl;
    }
}

int
opfront::balance_surface(int front_edge_links, int maximum_front_size)
{
#ifdef OPFRONT_DEBUG
    std::cout << "Balancing surface" << std::endl;
    boost::progress_timer timer;
    // Clean up 
#endif
    // Re-balance surface (note this is only balancing the part of the surface
    // that moved in the last iteration, not the complete front. This should be
    // okay as long as movement is detected correctly, however small drift
    // could perhaps go undetected and lead to an unbalanced surface over
    // time).
    subdivision::balance_length(surface, maximum_edge_length);
    // Extract front surfaces.
    processing_front = fronts = front_extract(surface, front_edge_links,
        maximum_front_size);
    int front_vertex_count = 0;
    for(GSList *f = fronts; f; f = f->next)
    {
        front_vertex_count += gts_surface_vertex_number(GTS_SURFACE(f->data));
    }
#ifdef OPFRONT_DEBUG
    std::cout << "Front vertices: " << front_vertex_count << std::endl;
#endif
    return front_vertex_count;
}

bool
opfront::next_front()
{
#ifdef OPFRONT_DEBUG
    std::cout << "Processing front" << std::endl;
    boost::progress_timer timer;
#endif
    // Clear previous front.
    front_vertices.clear(); front_edges.clear();

    if(!processing_front)
    {
#ifdef OPFRONT_DEBUG
        std::cout << " all completed" << std::endl;
#endif
        return false;
    }
    else
    {
        // And extract.
        front_copy(GTS_SURFACE(processing_front->data), front_vertices,
            front_edges);
        processing_front = processing_front->next;
#ifdef OPFRONT_DEBUG
        std::cout << " vertices: " << front_vertices.size() << std::endl;
#endif
        return true;
    }
}

void
opfront::compute_minimum_cut()
{
#ifdef OPFRONT_DEBUG
    std::cout << "Constructing graph" << std::endl;
#endif
    multi_graph<ofloat> graph(m_surface_count, front_vertices.size(),
        edge_cost, edge_cost_params, 0, 0, region_cost, region_cost_params,
        error_function);
    graph_setup(m_surface_count, &front_vertices[0], front_vertices.size(),
        &front_edges[0], front_edges.size(),
        graph, edge_offset, edge_offset_params, edge_constraint,
        edge_constraint_params, surface_cost, surface_cost_params);
#ifdef OPFRONT_DEBUG
    std::cout << "Calculating minimum cut" << std::endl;
#endif
#ifdef OPFRONT_PRINT_CUT_TIME
    boost::chrono::thread_clock::time_point start =
        boost::chrono::thread_clock::now();
#endif
    graph.calculate();
#ifdef OPFRONT_DEBUG
    std::cout << "Updating surfaces" << std::endl;
#endif
    //std::vector<std::vector<ofloat> > probability;
    if(m_min_marginals)
        graph_update_surface_with_min_marginals(m_surface_count,
            &front_vertices[0], front_vertices.size(),
            graph, m_min_marginals, mask, mask_params);
    else
        graph_update_surface(m_surface_count, &front_vertices[0],
            front_vertices.size(), graph, mask, mask_params);
#ifdef OPFRONT_PRINT_CUT_TIME
    std::cout << "Max-flow: " << graph.get_maxflow() << std::endl;
    boost::chrono::thread_clock::time_point stop =
        boost::chrono::thread_clock::now();
    std::cout << "Graph vertices: " << graph.node_count << " cut time: "
        << boost::chrono::duration_cast<boost::chrono::microseconds>(
            (stop-start)).count()/1000000.0 << " s" << std::endl;
#endif
}

static gint
vertex_moved_check(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    if(!m->moving) return 0;
    GNode *tree = static_cast<GNode*>(data);
    ofloat distance = gts_bb_tree_point_distance(tree, GTS_POINT(m),
        (GtsBBoxDistFunc) gts_point_triangle_distance2, 0);
    if(distance < movement_distance) m->moving = false;
    return 0;
}

void
opfront::stop_oscillerations()
{
#ifdef OPFRONT_DEBUG
    std::cout << "Stopping oscillerations" << std::endl;
    boost::progress_timer timer;
#endif
    for(std::list<GtsSurface*>::iterator i = previous_iterations.begin();
        i != previous_iterations.end(); i++)
    {
        GNode *tree = gts_bb_tree_surface(*i);
        gts_surface_foreach_vertex(surface, vertex_moved_check, tree);
        gts_bb_tree_destroy(tree, true);
    }
}

void
opfront::initialize(ofloat _maximum_edge_length)
{
    maximum_edge_length = _maximum_edge_length;
    front_initialize(surface, mask, mask_params);
    while(previous_iterations.size())
    {
        gts_object_destroy(GTS_OBJECT(previous_iterations.front()));
        previous_iterations.pop_front();
    }
    sub_iteration = 0;
}
    
double
opfront::label(int surface, int column_index) const
{
    return multi_surface_index(front_vertices[column_index], surface);
}

double
opfront::min_marginal_label(int surface, int column_index) const
{
    return multi_surface_min_marginal_index(front_vertices[column_index],
        surface);
}

opfront::opfront(GtsSurface *_surface,
    ofloat _maximum_edge_length,
    int (*_edge_offset)(int, int, int, int, void*),
    void *_edge_offset_params,
    void (*_edge_constraint)(int, int, int, int, int&, int&, void*),
    void *_edge_constraint_params,
    ofloat (*_edge_cost)(int, int, int, int, int, void*),
    void *_edge_cost_params,
    ofloat (*_surface_cost)(int, int, int, void*),
    void *_surface_cost_params,
    ofloat (*_region_cost)(int, int, int, void*),
    void *_region_cost_params,
    bool (*_mask)(GtsMVertex*, void*),
    void *_mask_params,
    int min_marginals,
    void (*_sub_iteration_done)(int, void*),
    void *_sub_iteration_done_params,
    void (*_error_function)(const char*)):
    surface(_surface),
    m_surface_count(multi_surface_count(_surface)),
    fronts(0),
    edge_offset(_edge_offset),
    edge_offset_params(_edge_offset_params),
    edge_constraint(_edge_constraint),
    edge_constraint_params(_edge_constraint_params),
    edge_cost(_edge_cost),
    edge_cost_params(_edge_cost_params),
    surface_cost(_surface_cost),
    surface_cost_params(_surface_cost_params),
    region_cost(_region_cost),
    region_cost_params(_region_cost_params),
    mask(_mask? _mask: mask_vertex),
    mask_params(_mask_params),
    m_min_marginals(min_marginals),
    sub_iteration_done(_sub_iteration_done),
    sub_iteration_done_params(_sub_iteration_done_params),
    error_function(_error_function)
{
    initialize(_maximum_edge_length);
}

opfront::~opfront()
{
    for(std::list<GtsSurface*>::iterator i = previous_iterations.begin();
        i != previous_iterations.end(); i++)
    {
        gts_object_destroy(GTS_OBJECT(*i));
    }
    if(fronts)
    {
        for(GSList *f = fronts; f; f = f->next)
            gts_object_destroy(GTS_OBJECT(f->data));
        g_slist_free(fronts);
        fronts = NULL;
    }
}

const gdouble*
opfront::column_position(int column_index, int node_index) const
{
    return mvertex_position(front_vertices[column_index], node_index);
}

int
opfront::column_count() const
{
    return front_vertices.size();
}

int
opfront::column_length(int column_index) const
{
    return mvertex_column_size(front_vertices[column_index]);
}
    
int
opfront::surface_node_index(int surface, int column_index) const
{
    return multi_surface_index(front_vertices[column_index], surface);
}

int
opfront::edge_count() const
{
    return front_edges.size();
}

void
opfront::edge(int edge_index, int &column_index_0, int &column_index_1)
{
    GtsEdge *e = front_edges[edge_index];
    column_index_0 = GTS_MVERTEX(e->segment.v1)->index;
    column_index_1 = GTS_MVERTEX(e->segment.v2)->index;
}

int
opfront::surface_count() const
{
    return m_surface_count;
}
    
int
opfront::front_number(GtsFace *f) const
{
    int c = 0;
    for(GSList *i = fronts; i; i = i->next)
    {
        if(gts_face_has_parent_surface(f, GTS_SURFACE(i->data)))
            return c;
        c++;
    }
    return -1;
}

