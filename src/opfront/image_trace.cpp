/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */

#include <iomanip>
#include <algorithm>
#include <iostream>
#ifdef OPFRONT_DEBUG
#include <boost/progress.hpp>
#endif
#include "opfront/image_trace.h"
#include "opfront/ball_search.h"
#include "util/kernel.h"
#include "util/fftconv.h"
#include "util/lalg.h"

#define FFT 1

const int image_trace_sub_sample = 3;

class image_trace_params
{
public:
    // potential images of region of interest.
    cimg_library::CImg<ofloat> p;
    ofloat direction;
};

template<class FLOAT_TYPE>
FLOAT_TYPE
potential_image_sample(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z, void *params)
{
    image_trace_params *data = static_cast<image_trace_params*>(params);
    data->p.xyz2ijk(x, y, z);
    return data->direction*data->p.cubic_atXYZ(x, y, z);
}
    
void
image_sample_generate_common_params(
    const fill<ofloat, ofloat, gts_voxelization>& filler,
    const std::vector<ofloat> &points,
    int kernel_size,
    ofloat kernel_spacing,
    image_trace_params &data)
{
    ofloat reach = (kernel_size/2)*kernel_spacing;
#ifdef OPFRONT_DEBUG
    std::cout << "Kernel reach: " << reach << std::endl;
    std::cout << "Kernel spacing: " << kernel_spacing << std::endl;
#endif
    // Compute region of interest.
    ofloat roi[] = {std::numeric_limits<ofloat>::max(),
                    std::numeric_limits<ofloat>::max(),
                    std::numeric_limits<ofloat>::max(),
                    -std::numeric_limits<ofloat>::max(),
                    -std::numeric_limits<ofloat>::max(),
                    -std::numeric_limits<ofloat>::max()};
    for(int i = 0; i < points.size(); i+=3)
    {
        for(int j = 0; j < 3; j++)
        {
            if(points[i+j] < roi[j]) roi[j] = points[i+j];
            if(points[i+j] > roi[j+3]) roi[j+3] = points[i+j];
        }
    }
    // Influence is limited to region within reach of the filter.
    for(int i = 0; i < 3; i++)
    {
        roi[i] -= reach;
        roi[i+3] += reach;
    }
    int ax = static_cast<int>(std::floor(roi[0]/kernel_spacing)),
        ay = static_cast<int>(std::floor(roi[1]/kernel_spacing)),
        az = static_cast<int>(std::floor(roi[2]/kernel_spacing));
    int mx = static_cast<int>(std::ceil(roi[3]/kernel_spacing)),
        my = static_cast<int>(std::ceil(roi[4]/kernel_spacing)),
        mz = static_cast<int>(std::ceil(roi[5]/kernel_spacing));
#ifdef OPFRONT_DEBUG
    std::cout << "Convolution size: " << mx-ax << "x" << my-ay << "x"
              << mz-az << std::endl;
#endif
    data.p.metadata.origin[0] = -ax*kernel_spacing;
    data.p.metadata.origin[1] = -ay*kernel_spacing;
    data.p.metadata.origin[2] = az*kernel_spacing;
    data.p.metadata.spacing[0] = -kernel_spacing;
    data.p.metadata.spacing[1] = -kernel_spacing;
    data.p.metadata.spacing[2] = kernel_spacing;

    data.p.assign(mx-ax, my-ay, mz-az, 1, 0);
    data.direction = 1;
#ifdef OPFRONT_DEBUG
    {
    boost::progress_timer timer;
    std::cout << "Extracting binary image... " << std::flush;
#endif
    // Create binary segmentation images in region of interest.
    filler.grow_sub(data.p, &points[0], points.size(), image_trace_sub_sample);
#ifdef OPFRONT_DEBUG
    std::cout << " Done!" << std::endl;
    }
#endif
}

void
image_sample_generate_potential_params(
    const fill<ofloat, ofloat, gts_voxelization>& filler,
    const std::vector<ofloat>& points,
    ofloat interval,
    cimg_library::CImg<double> &interval_values,
    image_trace_params& data,
    ofloat kernel_spacing,
    ofloat regularization,
    int kernel_size)
{
    int beta = 2;
#ifdef OPFRONT_DEBUG
    std::cout << "Flow line regularization: " << regularization << std::endl;
#endif
    image_sample_generate_common_params(filler, points, kernel_size,
        kernel_spacing, data);
    cimg_library::CImg<ofloat> kernel = potential<ofloat>(regularization,
        beta, kernel_size, kernel_spacing, kernel_spacing, kernel_spacing);
    {
#ifdef FFT
#ifdef OPFRONT_DEBUG
    std::cout << "Using FFT for convolution" << std::endl;
#endif
    fftconv(kernel, data.p);
#else
#ifdef OPFRONT_DEBUG
    std::cout << "Using standard convolution" << std::endl;
#endif
    data.p.convolve(kernel);
#endif
    }
    kernel = potential1d<ofloat>(regularization, beta, kernel_size,
        kernel_spacing);
#ifdef OPFRONT_DEBUG
    std::cout << "Kernel: ";
    cimg_forX(kernel, x) std::cout << kernel(x) << " ";
    std::cout << std::endl;
#endif
    // Make sure kernel size is un-even in order to sample the 0.5 level.
    kernel_size = static_cast<int>((kernel_spacing*kernel_size)/interval);
    kernel_size = static_cast<int>((kernel_size/2)*2+1);
    interval_values = potential1d<ofloat>(regularization,
        beta,
        kernel_size,
        interval);
}

void
image_sample_generate_deriche_params(
    const fill<ofloat, ofloat, gts_voxelization>& filler,
    const std::vector<ofloat>& points,
    ofloat interval,
    cimg_library::CImg<double> &interval_values,
    image_trace_params& data,
    ofloat kernel_spacing,
    ofloat regularization,
    int kernel_size)
{
#ifdef OPFRONT_DEBUG
    std::cout << "Flow line regularization: " << regularization << std::endl;
#endif
    image_sample_generate_common_params(filler, points, kernel_size,
        kernel_spacing, data); 
    cimg_library::CImg<ofloat> kernel =
        deriche<ofloat>(regularization, kernel_size, kernel_spacing,
            kernel_spacing, kernel_spacing);
    {
#ifdef FFT
#ifdef OPFRONT_DEBUG
    std::cout << "Using FFT for convolution" << std::endl;
#endif
    fftconv(kernel, data.p);
#else
#ifdef OPFRONT_DEBUG
    std::cout << "Using standard convolution" << std::endl;
#endif
    data.p.convolve(kernel);
#endif
    }
    kernel = deriche1d<ofloat>(regularization, kernel_size, kernel_spacing);
#ifdef OPFRONT_DEBUG
    std::cout << "Kernel: ";
    cimg_forX(kernel, x) std::cout << kernel(x) << " ";
    std::cout << std::endl;
#endif
    // Make sure kernel size is un-even in order to sample the 0.5 level.
    kernel_size = static_cast<int>((kernel_spacing*kernel_size)/interval);
    kernel_size = static_cast<int>((kernel_size/2)*2+1);

    interval_values = deriche1d<ofloat>(regularization, kernel_size,
        interval);
}

void
image_sample_generate_gaussian_params(
    const fill<ofloat, ofloat, gts_voxelization>& filler,
    const std::vector<ofloat>& points,
    ofloat interval,
    cimg_library::CImg<double> &interval_values,
    image_trace_params& data,
    ofloat kernel_spacing,
    ofloat regularization,
    int kernel_size)
{
#ifdef OPFRONT_DEBUG
    std::cout << "Flow line regularization: " << regularization << std::endl;
#endif
    image_sample_generate_common_params(filler, points, kernel_size,
        kernel_spacing, data); 
    cimg_library::CImg<ofloat>
        kernel_sx = gauss<ofloat>(regularization, kernel_size, kernel_spacing),
        kernel_sy = gauss<ofloat>(regularization, kernel_size, kernel_spacing),
        kernel_sz = gauss<ofloat>(regularization, kernel_size, kernel_spacing);
    const char yorder[] = "yxzc", zorder[] = "yzxc";
    kernel_sy.permute_axes(yorder);
    kernel_sz.permute_axes(zorder);
    data.p.convolve(kernel_sx);
    data.p.convolve(kernel_sy);
    data.p.convolve(kernel_sz);
#ifdef OPFRONT_DEBUG
    std::cout << "Kernel: ";
    cimg_forXYZ(kernel_sx, x, y, z)
        std::cout << kernel_sx(x, y, z) << " ";
    std::cout << std::endl;
#endif
    interval_values = gauss<ofloat>(regularization, static_cast<int>(
        kernel_spacing*kernel_size/interval), interval);
}

void
image_sample_generate_distance_params(
    const fill<ofloat, ofloat, gts_voxelization>& filler,
    const std::vector<ofloat>& points,
    ofloat interval,
    cimg_library::CImg<double> &interval_values,
    image_trace_params& data,
    ofloat kernel_spacing,
    ofloat regularization,
    int kernel_size)
{
    image_sample_generate_common_params(filler, points, kernel_size,
        kernel_spacing, data); 
    data.p -= 0.5;
    data.p.distance_eikonal(100);
}

image_tracer_type::image_tracer_type(
    ofloat regularization_, ofloat sample_interval_, ofloat kernel_spacing_,
    ofloat max_inner_, ofloat max_outer_, int type_, int kernel_size_) :
    regularization(regularization_), sample_interval(sample_interval_),
    kernel_spacing(kernel_spacing_), max_inner(max_inner_),
    max_outer(max_outer_), type(type_), kernel_size(kernel_size_)
{
}

void
image_tracer_type::update_surface(GtsSurface *s)
{
    filler.~fill();
    voxelizer.~gts_voxelization();
    new (&voxelizer) gts_voxelization(s);
    new (&filler) fill<ofloat, ofloat, gts_voxelization>(&voxelizer);
}

void
image_tracer_type::operator()(const std::vector<GtsMVertex*>& vertices) const
{
    assert(vertices.size());
    std::vector<ofloat> points(vertices.size()*3);
    for(int i = 0; i < points.size(); i += 3)
    {
        //points[i] = vertices[i/3]->parent.p.x;
        //points[i+1] = vertices[i/3]->parent.p.y;
        //points[i+2] = vertices[i/3]->parent.p.z;
        gdouble p[3];
        multi_surface_position(vertices[i/3], 0, p);
        points[i] = p[0];
        points[i+1] = p[1];
        points[i+2] = p[2];
        
    }
    cimg_library::CImg<double> interval_values;
    image_trace_params data;
    switch(type)
    {
    case 0:
        image_sample_generate_gaussian_params(filler, points, sample_interval,
            interval_values, data, kernel_spacing, regularization,
            kernel_size); 
        break;
    case 1:
        image_sample_generate_deriche_params(filler, points, sample_interval,
            interval_values, data, kernel_spacing, regularization,
            kernel_size);
        break;
    case 2:
        image_sample_generate_potential_params(filler, points, sample_interval,
            interval_values, data, kernel_spacing, regularization,
            kernel_size);
        break;
    case 3:
        image_sample_generate_distance_params(filler, points, sample_interval,
            interval_values, data, kernel_spacing, regularization,
            kernel_size);
        break;
    default:
        std::cerr << "ERROR: Unknown flow line type!" << std::endl;
        std::exit(1);
        break;
    }
#ifdef OPFRONT_DEBUG
    std::cout << "Tracing flow lines" << std::endl;
    boost::progress_timer timer;
#endif
    data.direction = 1;    
    std::vector<std::vector<ofloat> >
        flow_lines(vertices.size(), std::vector<ofloat>());
    std::vector<int> midpoints;
    midpoints.reserve(vertices.size());

    trace_batch(points, sample_interval, potential_image_sample, &data,
        max_inner, flow_lines);
    for(int i = 0; i < vertices.size(); i++)
    {
        // Order vertices from the inside and out.
        int e = flow_lines[i].size()-3;
        for(int j = 0; j < e; j+=3, e-=3)
            linalg_swa_ve3(&flow_lines[i][e], &flow_lines[i][j]);
        midpoints.push_back(flow_lines[i].size()/3);
        //flow_lines[i].push_back(points[i*3]);
        //flow_lines[i].push_back(points[i*3+1]);
        //flow_lines[i].push_back(points[i*3+2]);
    }
    data.direction = -1;
    trace_batch(points, sample_interval, potential_image_sample, &data,
        max_outer, flow_lines);
    int e = 0;
    for(int i = 0; i < vertices.size(); i++)
    {
        // Store the point if no column could be traced. Note this should not
        // happen often.
        if(!flow_lines[i].size())
        {
            mvertex_column(vertices[i], &points[i*3], 1);
            e++;
        }
        else
            mvertex_column(vertices[i], &flow_lines[i][0],
                           flow_lines[i].size()/3);
        multi_surface_index(vertices[i], 0, midpoints[i]);
    }
#ifdef OPFRONT_DEBUG
    if(e)
        std::cout << "WARNING: could not trace " << e 
            << " flow lines, using initial points!" << std::endl;
#endif
}

