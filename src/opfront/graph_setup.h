/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __GRAPH_SETUP_H__
#define __GRAPH_SETUP_H__

#include "opfront/defines.h"
#include "optisurf/mgraph.h"
#include "opfront/multi_surface.h"

void
graph_setup(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    GtsEdge **edges,
    int edge_count,
    multi_graph<ofloat>& graph,
    int (*edge_offset)(int, int, int, int, void*),
    void *edge_offset_params,
    void (*edge_constraint)(int, int, int, int, int&, int&, void*),
    void *edge_constraint_params,
    ofloat (*surface_cost)(int, int, int, void*),
    void *surface_cost_params);

void
graph_update_surface(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    multi_graph<ofloat> &graph,
    bool (*mask_vertex)(GtsMVertex*, void*),
    void *mask_vertex_params);

void
graph_update_surface_with_min_marginals(int surface_count,
    GtsMVertex **vertices,
    int vertex_count,
    multi_graph<ofloat> &graph,
    int min_marginals,
    bool (*mask_vertex)(GtsMVertex*, void*),
    void *mask_vertex_params);

#endif

