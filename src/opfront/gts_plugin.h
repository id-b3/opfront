/* Modified GTS library code
 * Copyright (C) 2014 Jens Petersen
 *
 * Based on:
 * GTS - Library for the manipulation of triangulated surfaces
 * Copyright (C) 1999 Stéphane Popine 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __gts_surface_plugin_h__
#define __gts_surface_plugin_h__

#include "opfront/multi_surface.h"

GtsSurface*
gts_surface_is_self_intersecting_front(GtsSurface *s);

void
gts_surface_coarsen_front(GtsSurface *surface,
    GtsKeyFunc cost_func,
    gpointer cost_data, 
    GtsCoarsenFunc coarsen_func,
    gpointer coarsen_data,
    GtsStopFunc stop_func,
    gpointer stop_data,
    gdouble minangle);

void
gts_surface_refine_front(GtsSurface *surface,
    GtsKeyFunc cost_func,
    gpointer cost_data,
    GtsRefineFunc refine_func,
    gpointer refine_data,
    GtsStopFunc stop_func,
    gpointer stop_data);

#endif

