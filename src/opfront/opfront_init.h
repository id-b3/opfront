/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef OPFRONT_INIT
#define OPFRONT_INIT

#include "util/gts_util.h"

template<class CIMG_TYPE, class FLOAT_TYPE>
GtsSurface*
opfront_init_sphere(cimg_library::CImg<CIMG_TYPE>& img,
    FLOAT_TYPE x,
    FLOAT_TYPE y,
    FLOAT_TYPE z,
    FLOAT_TYPE radius,
    int surface_count)
{
#ifdef OPFRONT_DEBUG
    std::cout << "Generating " << surface_count
        << " surface(s) from sphere with center: "
        << x << ", " << y << ", " << z << ") and radius: "
        << radius << std::endl;
#endif
    if(x+radius < 0 || x-radius >= img.width() ||
       y+radius < 0 || y-radius >= img.height() ||
       z+radius < 0 || z-radius >= img.depth())
    {
        std::cout << "Error: sphere out of image bounds" << std::endl;
        return 0;
    }
    img.ijk2xyz(x, y, z);
    GtsSurface *surface = gts_surface_new(gts_surface_class(), 
        gts_face_class(), gts_edge_class(),
        GTS_VERTEX_CLASS(gts_mvertex_class())); 
    gts_surface_generate_sphere(surface, 5);
    gts_util_multiply_s(surface, radius);
    gdouble center[] = {x, y, z};
    gts_util_add(surface, center);
    multi_surface(surface, surface_count);
    multi_surface_select(surface, 0);
    return surface;
}


template<class CIMG_TYPE>
GtsSurface*
opfront_init_gts(cimg_library::CImg<CIMG_TYPE>& img,
    const std::string& filename,
    int surface_count)
{
#ifdef OPFRONT_DEBUG
    std::cout << "Generating " << surface_count
        << " surface(s) from gts file " << std::flush;
#endif
    GtsSurface *surface = gts_surface_new(gts_surface_class(), 
        gts_face_class(), gts_edge_class(),
        GTS_VERTEX_CLASS(gts_mvertex_class())); 
    FILE *pfile = fopen(filename.c_str(), "r");
    GtsFile *fp = gts_file_new(pfile);
    int err = gts_surface_read(surface, fp);
    gts_file_destroy(fp);
    fclose(pfile);
#ifdef OPFRONT_DEBUG
    std::cout << "containing: "
        << gts_surface_vertex_number(surface) << " vertices "
        << gts_surface_edge_number(surface) << " edges!"
        << std::endl;
#endif
    multi_surface(surface, surface_count);
    multi_surface_meta_ijk2xyz(img, surface);
    multi_surface_select(surface, 0);
    return surface;
}

#endif

