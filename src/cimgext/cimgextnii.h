/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
public:
    bool
    load_nii(const char *filename)
    {
        nifti_image *nii = nifti_image_read(filename, true);
        //nifti_1_header *hdr = nifti_read_header(filename.c_str(), 0, 0);
        //load(filename);
        //std::cout << hdr->qoffset_x << " " << hdr->qoffset_y << " "
        //          << hdr->qoffset_z << " - " << hdr->quatern_b << " "
        //          << hdr->quatern_c << " " << hdr->quatern_d << std::endl;
        int nt = nii->nt > nii->nu? nii->nt: nii->nu;
        assign(nii->nx, nii->ny, nii->nz, nt);
        metadata.origin[0] = -nii->qoffset_x;
        metadata.origin[1] = -nii->qoffset_y;
        metadata.origin[2] = nii->qoffset_z;
        metadata.spacing[0] = nii->dx;
        metadata.spacing[1] = nii->dy;
        metadata.spacing[2] = nii->dz;
        for(int x = 0; x < nii->nx; x++)
            for(int y = 0; y < nii->ny; y++)
                for(int z = 0; z < nii->nz; z++)
                    for(int t = 0; t < nt; t++)
        {
            switch(nii->datatype)
            {
            case NIFTI_TYPE_INT8:
                (*this)(x, y, z, t) = static_cast<T>(
                    static_cast<char*>(nii->data)[
                    t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            case NIFTI_TYPE_UINT8:
                (*this)(x, y, z, t) = static_cast<T>(
                    static_cast<unsigned char*>(nii->data)[
                    t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            case NIFTI_TYPE_UINT16:
                (*this)(x, y, z, t) = static_cast<T>(
                    static_cast<unsigned short*>(nii->data)[
                    t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            case NIFTI_TYPE_INT16:
                (*this)(x, y, z, t) = static_cast<T>(static_cast<short*>(
                    nii->data)[t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            case NIFTI_TYPE_FLOAT32:
                (*this)(x, y, z, t) = static_cast<T>(static_cast<float*>(
                    nii->data)[t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            case NIFTI_TYPE_FLOAT64:
                (*this)(x, y, z, t) = static_cast<T>(static_cast<double*>(
                    nii->data)[t*nii->nz*nii->ny*nii->nx+
                    z*nii->ny*nii->nx+
                    y*nii->nx+
                    x]);
                break;
            default:
                std::cerr << "ERROR: Unknown nifti datatype!" << std::endl;
                std::exit(1);
                break;
            }
        }
        nifti_image_free(nii);
        return true;
    }

    bool
    save_nii(const char *filename) const
    {
        nifti_image nii;
        // analyze75_orient
        nii.aux_file[0] = 0;
        nii.byteorder = 1;
        nii.cal_max = 0;
        nii.cal_min = 0;
        if(typeid(T()) == typeid(unsigned char()))
        {
            nii.datatype = NIFTI_TYPE_UINT8;
        }
        else if(typeid(T()) == typeid(short()))
        {
            nii.datatype = NIFTI_TYPE_INT16;
        }
        else if(typeid(T()) == typeid(float()))
        {
            nii.datatype = NIFTI_TYPE_FLOAT32;
        }
        else if(typeid(T()) == typeid(double()))
        {
            nii.datatype = NIFTI_TYPE_FLOAT64;
        }
        else assert(0);
        nii.descrip[0] = 0;
        if(spectrum() > 1) nii.dim[0] = 4;
        else if(depth() > 1) nii.dim[0] = 3;
        else if(height() > 1) nii.dim[0] = 2;
        else if(width() > 1) nii.dim[0] = 1;
        else nii.dim[0] = 0;
        nii.dim[1] = width();
        nii.dim[2] = height();
        nii.dim[3] = depth();
        nii.dim[4] = spectrum();
        nii.dim[5] = 1;
        nii.dim[6] = 1;
        nii.dim[7] = 1;
        nii.dt = 0;
        nii.du = 0;
        nii.dv = 0;
        nii.dw = 0;
        nii.dx = metadata.spacing[0];
        nii.dy = metadata.spacing[1];
        nii.dz = metadata.spacing[2];
        // ext_list
        nii.fname = const_cast<char*>(filename);
        nii.freq_dim = 0;
        nii.iname = const_cast<char*>(filename);
        nii.iname_offset = nii.nz+1;
        nii.intent_code = 0;
        nii.intent_name[0] = 0;
        nii.intent_p1 = 0;
        nii.intent_p2 = 0;
        nii.intent_p3 = 0;
        nii.nbyper = sizeof(T);
        nii.ndim = nii.dim[0];
        nii.nifti_type = 1;
        nii.nt = spectrum(); 
        nii.nu = 1;
        nii.num_ext = 0;
        nii.nv = 1;
        nii.nvox = width()*height()*depth()*spectrum();
        nii.nw = 1;
        nii.nx = width(); 
        nii.ny = height(); 
        nii.nz = depth();
        nii.phase_dim = 0;
        nii.pixdim[0] = 0;
        nii.pixdim[1] = nii.dx;
        nii.pixdim[2] = nii.dy;
        nii.pixdim[3] = nii.dz;
        nii.pixdim[4] = nii.dt;
        nii.pixdim[5] = nii.du;
        nii.pixdim[6] = nii.dv;
        nii.pixdim[7] = nii.dw;
        nii.qfac = 0;
        nii.qform_code = 2;
        nii.qoffset_x = -metadata.origin[0];
        nii.qoffset_y = -metadata.origin[1];
        nii.qoffset_z = metadata.origin[2];
        nii.qto_ijk.m[0][0] = -1.0/metadata.spacing[0];
        nii.qto_ijk.m[0][1] = 0;
        nii.qto_ijk.m[0][2] = 0;
        nii.qto_ijk.m[0][3] = -metadata.origin[0]/metadata.spacing[0];
        nii.qto_ijk.m[1][0] = 0;
        nii.qto_ijk.m[1][1] = -1.0/metadata.spacing[1];
        nii.qto_ijk.m[1][2] = 0;
        nii.qto_ijk.m[1][3] = -metadata.origin[1]/metadata.spacing[1];
        nii.qto_ijk.m[2][0] = 0;
        nii.qto_ijk.m[2][1] = 0;
        nii.qto_ijk.m[2][2] = 1.0/metadata.spacing[2];
        nii.qto_ijk.m[2][3] = -metadata.origin[2]/metadata.spacing[2];
        nii.qto_ijk.m[3][0] = 0;
        nii.qto_ijk.m[3][1] = 0;
        nii.qto_ijk.m[3][2] = 0;
        nii.qto_ijk.m[3][3] = 1;
        nii.qto_xyz.m[0][0] = -metadata.spacing[0];
        nii.qto_xyz.m[0][1] = 0;
        nii.qto_xyz.m[0][2] = 0;
        nii.qto_xyz.m[0][3] = -metadata.origin[0];
        nii.qto_xyz.m[1][0] = 0;
        nii.qto_xyz.m[1][1] = -metadata.spacing[1];
        nii.qto_xyz.m[1][2] = 0;
        nii.qto_xyz.m[1][3] = -metadata.origin[1];
        nii.qto_xyz.m[2][0] = 0;
        nii.qto_xyz.m[2][1] = 0;
        nii.qto_xyz.m[2][2] = metadata.spacing[2];
        nii.qto_xyz.m[2][3] = metadata.origin[2];
        nii.qto_xyz.m[3][0] = 0;
        nii.qto_xyz.m[3][1] = 0;
        nii.qto_xyz.m[3][2] = 0;
        nii.qto_xyz.m[3][3] = 1;
        nii.quatern_b = 0;
        nii.quatern_c = 0;
        nii.quatern_d = 1;
        nii.scl_inter = 0;
        nii.scl_slope = 1;
        nii.sform_code = 1;
        nii.slice_code = 0;
        nii.slice_dim = 0;
        nii.slice_duration = 0;
        nii.slice_end = 0;
        nii.slice_start = 0;
        nii.sto_ijk.m[0][0] = -1.0/metadata.spacing[0];
        nii.sto_ijk.m[0][1] = 0;
        nii.sto_ijk.m[0][2] = 0;
        nii.sto_ijk.m[0][3] = -metadata.origin[0]/metadata.spacing[0];
        nii.sto_ijk.m[1][0] = 0;
        nii.sto_ijk.m[1][1] = -1.0/metadata.spacing[1];
        nii.sto_ijk.m[1][2] = 0;
        nii.sto_ijk.m[1][3] = -metadata.origin[1]/metadata.spacing[1];
        nii.sto_ijk.m[2][0] = 0;
        nii.sto_ijk.m[2][1] = 0;
        nii.sto_ijk.m[2][2] = 1.0/metadata.spacing[2];
        nii.sto_ijk.m[2][3] = -metadata.origin[2]/metadata.spacing[2];
        nii.sto_ijk.m[3][0] = 0;
        nii.sto_ijk.m[3][1] = 0;
        nii.sto_ijk.m[3][2] = 0;
        nii.sto_ijk.m[3][3] = 1;
        nii.sto_xyz.m[0][0] = -metadata.spacing[0];
        nii.sto_xyz.m[0][1] = 0;
        nii.sto_xyz.m[0][2] = 0;
        nii.sto_xyz.m[0][3] = -metadata.origin[0];
        nii.sto_xyz.m[1][0] = 0;
        nii.sto_xyz.m[1][1] = -metadata.spacing[1];
        nii.sto_xyz.m[1][2] = 0;
        nii.sto_xyz.m[1][3] = -metadata.origin[1];
        nii.sto_xyz.m[2][0] = 0;
        nii.sto_xyz.m[2][1] = 0;
        nii.sto_xyz.m[2][2] = metadata.spacing[2];
        nii.sto_xyz.m[2][3] = metadata.origin[2];
        nii.sto_xyz.m[3][0] = 0;
        nii.sto_xyz.m[3][1] = 0;
        nii.sto_xyz.m[3][2] = 0;
        nii.sto_xyz.m[3][3] = 1;
        nii.swapsize = 0;
        nii.time_units = 0;
        nii.toffset = 0;
        nii.xyz_units = 2;

        nii.data = new T[nii.nx*nii.ny*nii.nz*nii.nt];
        for(int x = 0; x < nii.nx; x++)
            for(int y = 0; y < nii.ny; y++)
                for(int z = 0; z < nii.nz; z++)
                    for(int t = 0; t < nii.nt; t++)
        {
            static_cast<T*>(nii.data)[
                t*nii.nz*nii.ny*nii.nx+
                z*nii.ny*nii.nx+
                y*nii.nx+
                x] = (*this)(x, y, z, t);
        }
        nifti_image_write(&nii);
        delete[] static_cast<T*>(nii.data);
        return true;
    }

    CImg&
    load_plugin_nii(const char *filename)
    {
        if(!load_nii(filename))
        {
            throw CImgIOException(
                "CImg<%s>::load_plugin_nii() : Error loading %s!",
                pixel_type(), filename);
        }
        return *this;
    }

    const CImg&
    save_plugin_nii(const char *filename) const
    {
        if(!save_nii(filename))
        {
            throw CImgIOException(
                "CImg<%s>::save_plugin_nii() : Error saving %s!",
                pixel_type(), filename);
        }
        return *this;
    }

