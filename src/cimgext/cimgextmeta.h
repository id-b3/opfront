/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
public:
	struct
    {
		double origin[3], spacing[3];
	} metadata;

    template<typename IMGTYPE>
    void
    set_metadata(const IMGTYPE& img)
    {
        for(int i = 0; i < 3; i++)
        {
            metadata.origin[i] = img.metadata.origin[i];
            metadata.spacing[i] = img.metadata.spacing[i];
        }
    }
    
    template<class FLOAT_TYPE>
    void
    xyz2ijk(FLOAT_TYPE &x, FLOAT_TYPE &y, FLOAT_TYPE &z) const
    {
        assert(metadata.spacing[0] && metadata.spacing[1] &&
            metadata.spacing[2]);
        x = -(x+metadata.origin[0])/metadata.spacing[0];
        y = -(y+metadata.origin[1])/metadata.spacing[1];
        z = (z-metadata.origin[2])/metadata.spacing[2];
    }
    
    template<class FLOAT_TYPE>
    void
    xyz2ijk(FLOAT_TYPE *p) const
    {
        xyz2ijk(p[0], p[1], p[2]);
    }
    
    template<class FLOAT_TYPE>
    void
    ijk2xyz(FLOAT_TYPE &i, FLOAT_TYPE &j, FLOAT_TYPE &k) const
    {
        i = -metadata.spacing[0]*i-metadata.origin[0];
        j = -metadata.spacing[1]*j-metadata.origin[1];
        k = metadata.spacing[2]*k+metadata.origin[2];
    }

    template<class FLOAT_TYPE>
    void
    ijk2xyz(FLOAT_TYPE *p) const
    {
        ijk2xyz(p[0], p[1], p[2]);
    }
     
