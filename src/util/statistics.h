/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __STATISTICS_H__
#define __STATISTICS_H__

#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

namespace statistics
{
    template<class TYPE>
    TYPE
    sum(const std::vector<TYPE>& values)
    {
        TYPE sum = 0;
        for(int i = 0; i < values.size(); i++) sum += values[i];
        return sum;
    }

    template<class TYPE>
    TYPE
    mean(const std::vector<TYPE>& values)
    {
        return sum(values)/values.size();
    }

    template<class TYPE>
    TYPE
    max(const std::vector<TYPE>& values)
    {
        TYPE m = -std::numeric_limits<TYPE>::max();
        for(int i = 0; i < values.size(); i++)
        {
            if(values[i] > m) m = values[i];
        }
        return m;
    }

    template<class TYPE>
    TYPE
    min(const std::vector<TYPE>& values)
    {
        TYPE m = std::numeric_limits<TYPE>::max();
        for(int i = 0; i < values.size(); i++)
        {
            if(values[i] < m) m = values[i];
        }
        return m;
    }

    template<class TYPE>
    TYPE
    stddev(const std::vector<TYPE>& values)
    {
        TYPE mn = mean(values), sdsum = 0;
        for(int i = 0; i < values.size(); i++)
        {
            sdsum += (values[i]-mn)*(values[i]-mn);
        }
        return sqrt(sdsum/values.size());
    }

    template<class TYPE>
    std::string
    summary(const std::vector<TYPE>& values)
    {
        std::stringstream ss;
        ss << "min: " << statistics::min(values)
           << " max: " << statistics::max(values)
           << " avg: " << statistics::mean(values)
           << " stddev: " << statistics::stddev(values);
        return ss.str();
    }
}

#endif

