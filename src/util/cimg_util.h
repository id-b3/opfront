/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __CIMG_UTIL_H__
#define __CIMG_UTIL_H__

// Windowize
template<class CIMG_TYPE_IN, class CIMG_TYPE_OUT>
cimg_library::CImg<CIMG_TYPE_OUT> 
windowize(const cimg_library::CImg<CIMG_TYPE_IN>& image,
		  const CIMG_TYPE_IN lower, const CIMG_TYPE_IN upper)
{
	cimg_library::CImg<CIMG_TYPE_OUT> ret;
	ret.assign(image.width(), image.height(), image.depth(), image.spectrum());
	cimg_forXYZC(image, x, y, z, c)
	{
        double v;
        if(image(x, y, z, c) < lower) v = lower;
        else if(image(x, y, z, c) > upper) v = upper;
        else v = image(x, y, z, c);
        ret(x, y, z, c) = static_cast<double>(v-lower)/(upper-lower)*
            std::numeric_limits<CIMG_TYPE_OUT>::max();
	}
	return ret;
}

// Normalize
//  Returns an image with the type changed and color ranges rescaled of to the
//  interval [lower, upper].
template<class CIMG_TYPE_IN, class CIMG_TYPE_OUT>
cimg_library::CImg<CIMG_TYPE_OUT> 
normalize(const cimg_library::CImg<CIMG_TYPE_IN>& image,
		  const CIMG_TYPE_OUT lower, const CIMG_TYPE_OUT upper)
{
	cimg_library::CImg<CIMG_TYPE_OUT> ret;
	ret.assign(image.width(), image.height(), image.depth(),
        image.spectrum());
	CIMG_TYPE_OUT interval = upper-lower;
	cimg_forC(image, v)
	{
		CIMG_TYPE_IN max_v = std::numeric_limits<CIMG_TYPE_IN>::min(),
					 min_v = std::numeric_limits<CIMG_TYPE_IN>::max();
		cimg_forXYZ(image, x, y, z)
		{
			if(image(x, y, z, v) > max_v) max_v = image(x, y, z, v);
			if(image(x, y, z, v) < min_v) min_v = image(x, y, z, v);
		}
		CIMG_TYPE_IN delta_v = max_v-min_v;
		if(!delta_v) delta_v = 1;
		cimg_forXYZ(image, x, y, z)
		{
			ret(x, y, z, v) = lower+(interval*(image(x, y, z, v)-min_v))/
							  delta_v;
		}
	}
	return ret;
}

// Normalize
//  Returns an image with the type changed and color ranges rescaled to the
//  interval [lower, upper].
template<class CIMG_TYPE_IN, class CIMG_TYPE_OUT>
void
normalize(const cimg_library::CImg<CIMG_TYPE_IN>& in, 
		  cimg_library::CImg<CIMG_TYPE_OUT>& out,
		  const CIMG_TYPE_OUT lower, const CIMG_TYPE_OUT upper)
{
	CIMG_TYPE_OUT interval = upper-lower;
	cimg_forC(in, v)
	{
		CIMG_TYPE_IN max_v = std::numeric_limits<CIMG_TYPE_IN>::min(),
					 min_v = std::numeric_limits<CIMG_TYPE_IN>::max();
		cimg_forXYZ(in, x, y, z)
		{
			if(in(x, y, z, v) > max_v) max_v = in(x, y, z, v);
			if(in(x, y, z, v) < min_v) min_v = in(x, y, z, v);
		}
		CIMG_TYPE_IN delta_v = max_v-min_v;
		if(!delta_v) delta_v = 1;
		cimg_forXYZ(in, x, y, z)
		{
			out(x, y, z, v) = lower+(interval*(in(x, y, z, v)-min_v))/
							  delta_v;
		}
	}
}

namespace
{
    template<class CIMG_TYPE>
    inline void
    add_point(cimg_library::CImg<CIMG_TYPE>& image,
              cimg_library::CImg<unsigned char>& background, unsigned* seeds,
              unsigned& index, unsigned x, unsigned y, unsigned z,
              CIMG_TYPE color)
    {
        if(x >= 0 && x < image.width() && y >= 0 && y < image.height() &&
           z >= 0 && z < image.depth() && image(x, y, z) == color && 
           !background(x, y, z))
        {
            seeds[index++] = z*image.height()*image.width()+y*image.width()+x;
            background(x, y, z) = 1;
        }
    }

    template<class CIMG_TYPE>
    inline void
    add_point_bitmask(cimg_library::CImg<CIMG_TYPE>& image,
        cimg_library::CImg<unsigned char>& background, unsigned* seeds,
        unsigned& index, unsigned x, unsigned y, unsigned z,
        unsigned char v, CIMG_TYPE b)
    {
        if(x >= 0 && x < image.width() && y >= 0 && y < image.height() &&
           z >= 0 && z < image.depth() && (image(x, y, z, v)&b) != b && 
           !background(x, y, z))
        {
            seeds[index++] = z*image.height()*image.width()+y*image.width()+x;
            background(x, y, z) = 1;
        }
    }
};

// Remove holes 2d
//  Removes the holes in the color channel v, with foreground bitmasks b.
template<class CIMG_TYPE>
int
remove_holes_2d(cimg_library::CImg<CIMG_TYPE>& image, unsigned char v,
                CIMG_TYPE b)
{
    unsigned dx = image.width(), dy = image.height(),
             *seeds = new unsigned[dx*dy], index = 0;
    cimg_library::CImg<unsigned char> background(dx, dy);
    background.fill(0);
    for(unsigned y = 0; y < dy; y++)
    {
        add_point_bitmask(image, background, seeds, index, 0, y, 0, v, b);
        add_point_bitmask(image, background, seeds, index, dx-1, y, 0, v, b);
    }
    for(unsigned x = 0; x < dx; x++)
    {
        add_point_bitmask(image, background, seeds, index, x, 0, 0, v, b);
        add_point_bitmask(image, background, seeds, index, x, dy-1, 0, v, b);
    }
    while(index--)
    {
        unsigned seed = seeds[index], y = seed/dx, x = seed-y*dx;
        add_point_bitmask(image, background, seeds, index, x-1, y, 0, v, b);
        add_point_bitmask(image, background, seeds, index, x+1, y, 0, v, b);
        add_point_bitmask(image, background, seeds, index, x, y-1, 0, v, b);
        add_point_bitmask(image, background, seeds, index, x, y+1, 0, v, b);
    }
    int count = 0;
    cimg_forXY(image, x, y)
    {
        if(!background(x, y) && (image(x, y, 0, v)&b) != b)
        {
            image(x, y, 0, v) |= b;
            count++;
        }
    }
    delete[] seeds;
    return count;
}


// Removes holes
//  Remove all the holes having the specified background color in the object
//  with the given foreground color.
template<class CIMG_TYPE>
int
remove_holes(cimg_library::CImg<CIMG_TYPE>& image, CIMG_TYPE background_color,
             CIMG_TYPE foreground_color)
{
    unsigned dx = image.width(), dy = image.height(), dz = image.depth(),
             *seeds = new unsigned[dx*dy*dz], index = 0;
    cimg_library::CImg<unsigned char> background(dx, dy, dz);
    background.fill(0);
    for(unsigned x = 0; x < dx; x++)
        for(unsigned y = 0; y < dy; y++)
    {
        add_point(image, background, seeds, index, x, y, 0, background_color);
        add_point(image, background, seeds, index, x, y, dz-1,
                  background_color);
    }
    for(unsigned y = 0; y < dy; y++)
        for(unsigned z = 0; z < dz; z++)
    {
        add_point(image, background, seeds, index, 0, y, z, background_color);
        add_point(image, background, seeds, index, dx-1, y, z,
                  background_color);
    }
    for(unsigned x = 0; x < dx; x++)
        for(unsigned z = 0; z < dz; z++)
    {
        add_point(image, background, seeds, index, x, 0, z, background_color);
        add_point(image, background, seeds, index, x, dy-1, z,
                  background_color);
    }
    while(index--)
    {
        unsigned seed = seeds[index], z = seed/(dx*dy), y = (seed-z*dx*dy)/dx,
                 x = seed-z*dx*dy-y*dx;
        add_point(image, background, seeds, index, x-1, y, z,
                  background_color);
        add_point(image, background, seeds, index, x+1, y, z,
                  background_color);
        add_point(image, background, seeds, index, x, y-1, z,
                  background_color);
        add_point(image, background, seeds, index, x, y+1, z,
                  background_color);
        add_point(image, background, seeds, index, x, y, z-1,
                  background_color);
        add_point(image, background, seeds, index, x, y, z+1,
                  background_color);
    }
    int count = 0;
    cimg_forXYZ(image, x, y, z)
    {
        if(!background(x, y, z) && image(x, y, z) == background_color)
        {
            image(x, y, z) = foreground_color;
            count++;
        }
    }
    delete[] seeds;
    return count;
}

// Remove tunnels
//  Fills all background color voxels with the foreground color if they
//  have less than the specified number of background color edge neighbours.
template<class CIMG_TYPE>
int
remove_tunnels(cimg_library::CImg<CIMG_TYPE>& image, int neighbour_count,
               CIMG_TYPE background_color, CIMG_TYPE foreground_color)
{
    int count = 0;
    unsigned *seeds =
        new unsigned[image.width()*image.height()*image.depth()],
        seed_count = 0;
    cimg_library::CImg<unsigned char> background(image.width(),
        image.height(), image.depth());
    background.fill(0);
    cimg_forXYZ(image, x, y, z)
        if(image(x, y, z) == background_color)
            add_point(image, background, seeds, seed_count, x, y, z,
                background_color);
    while(seed_count--)
    {
        unsigned seed = seeds[seed_count],
            z = seed/(image.width()*image.height()),
            y = (seed-z*image.width()*image.height())/image.width(),
            x = seed-z*image.width()*image.height()-y*image.width();
        if(image(x, y, z) == background_color)
        {
            int n = 0;
            if(!x || image(x-1, y, z) == background_color) n++;
            if(x == image.width()-1 || image(x+1, y, z) == background_color)
                n++;
            if(!y || image(x, y-1, z) == background_color)
                n++;
            if(y == image.height()-1 || image(x, y+1, z) == background_color)
                n++;
            if(!z || image(x, y, z-1) == background_color) n++;
            if(z == image.depth()-1 || image(x, y, z+1) == background_color)
                n++;
            if(n < neighbour_count)
            {
                count++;
                image(x, y, z) = foreground_color;
                add_point(image, background, seeds, seed_count, x-1, y, z,
                          background_color);
                add_point(image, background, seeds, seed_count, x+1, y, z,
                          background_color);
                add_point(image, background, seeds, seed_count, x, y-1, z,
                          background_color);
                add_point(image, background, seeds, seed_count, x, y+1, z,
                          background_color);
                add_point(image, background, seeds, seed_count, x, y, z-1,
                          background_color);
                add_point(image, background, seeds, seed_count, x, y, z+1,
                          background_color);
            }
        }
        background(x, y, z) = 0;
    }
    delete[] seeds;
    return count;
}

#endif

