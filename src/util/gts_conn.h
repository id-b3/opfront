#ifndef __GTS_CONN_H__
#define __GTS_CONN_H__

#include <gts.h>

GSList*
gts_connected_components(GtsSurface *s);

#endif

