#include <iostream>
#include <vector>
#include "gts.h"

void
filo_append(std::vector<GtsTriangle*> &filo, GSList *b)
{
    while(b)
    {
        filo.push_back(GTS_TRIANGLE(b->data));
        b = b->next;
    }
}

static void
traverse_manifold(GtsTriangle *triangle, GtsSurface *component,
    GtsSurface *parent)
{
    //GSList *queue = g_slist_append(NULL, triangle);
    std::vector<GtsTriangle*> filo;
    filo.push_back(triangle);
    while(filo.size())
    {
        GtsTriangle *t = filo.back();
        filo.pop_back();
        // Already visited this one.
        if(gts_face_has_parent_surface(GTS_FACE(t), component)) continue;
        // Moved outside parent surface.
        if(!gts_face_has_parent_surface(GTS_FACE(t), parent)) continue;
        // Add face and continue with neighbours.
        gts_surface_add_face(component, GTS_FACE(t));
        filo_append(filo, t->e1->triangles);
        filo_append(filo, t->e2->triangles);
        filo_append(filo, t->e3->triangles);
    }
}

static void
traverse(GtsFace *f, gpointer *data)
{
    GtsSurface *orig = GTS_SURFACE(data[0]);
    GSList **components = static_cast<GSList**>(data[1]);
    GSList *i = *components;
    while(i)
    {
        if(gts_face_has_parent_surface(f, GTS_SURFACE(i->data))) return;
        i = i->next;
    }
    GtsSurface *s = gts_surface_new(GTS_SURFACE_CLASS(GTS_OBJECT(orig)->klass),
        orig->face_class, orig->edge_class, orig->vertex_class);
    *components = g_slist_prepend(*components, s);
    traverse_manifold(GTS_TRIANGLE(f), s, orig);
}

GSList*
gts_connected_components(GtsSurface *s)
{
    gpointer data[2];
    GSList *components = NULL;
    g_return_val_if_fail(s != NULL, NULL);
    data[0] = s;
    data[1] = &components;
    gts_surface_foreach_face(s, (GtsFunc)traverse, data);
    return components;
}
