/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <GL/glu.h>
#include <GL/gl.h>
#include <iostream>
#include "util/lalg.h"

class camera
{
private:
    double position[3];
    double forward[3];
    double along[3]; 
    double up[3];
    
    void
    rotate_about(double angle, const double *v1, double *v2, double *v3)
    {
        linalg_mul_scl_ve3(cos(angle), v2);
        linalg_mul_scl_ve3(sin(angle), v3);
        linalg_add_ve3_ve3(v3, v2);
        linalg_nor_ve3(v2);
        linalg_crs_ve3(v2, v1, v3);
        linalg_mul_scl_ve3(-1, v3);
    }
    
    void
    move_along(double distance, const double *direction)
    {
        double t[] = {direction[0], direction[1], direction[2]};
        linalg_mul_scl_ve3(distance, t);
        linalg_add_ve3_ve3(t, position);
    }

public:
    void
    reset()
    {
        position[0] = 0; position[1] = 0; position[2] = 0;
        along[0] = 1; along[1] = 0; along[2] = 0;
        forward[0] = 0; forward[1] = 0; forward[2] = 1;
        up[0] = 0; up[1] = 1; up[2] = 0;
    }

    camera()
    {
        reset();
    }

    void
    print()
    {
        std::cout << "Forward: " << forward[0] << " " << forward[1] << " "
                  << forward[2] << std::endl;
        std::cout << "Up: " << up[0] << " " << up[1] << " " << up[2]
                  << std::endl;
        std::cout << "Along: " << along[0] << " " << along[1] << " "
                  << along[2] << std::endl;
        std::cout << "Position: " << position[0] << " " << position[1] << " "
                  << position[2] << std::endl;
    }

    void
    get_forward(double &v0, double &v1, double &v2) const
    {
        v0 = forward[0]; v1 = forward[1]; v2 = forward[2];
    }
    
    void
    set_forward(double v0, double v1, double v2)
    {
        forward[0] = v0; forward[1] = v1; forward[2] = v2;
    }

    void
    get_up(double &v0, double &v1, double &v2) const
    {
        v0 = up[0]; v1 = up[1]; v2 = up[2];
    }

    void
    set_up(double v0, double v1, double v2)
    {
        up[0] = v0; up[1] = v1; up[2] = v2;
    }

    void
    get_along(double &v0, double &v1, double &v2) const
    {
        v0 = along[0]; v1 = along[1]; v2 = along[2];
    }

    void 
    set_along(double v0, double v1, double v2)
    {
        along[0] = v0; along[1] = v1; along[2] = v2;
    }
    
    void
    get_position(double &v0, double &v1, double &v2) const
    {
        v0 = position[0]; v1 = position[1]; v2 = position[2];
    }

    void
    set_position(double v0, double v1, double v2)
    {
        position[0] = v0; position[1] = v1; position[2] = v2;
    }

    void
    update()
    {
        double x = linalg_dot_ve3(along, position);
        double y = linalg_dot_ve3(up, position);
        double z = linalg_dot_ve3(forward, position);
        double view[] = {along[0], up[0], forward[0], 0,
                         along[1], up[1], forward[1], 0,
                         along[2], up[2], forward[2], 0,
                                x,     y,          z, 1};
        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixd(view);
    }
    
    void
    yaw(double angle)
    {
        rotate_about(angle, up, along, forward);
    }

    void
    pitch(double angle)
    {
        rotate_about(angle, along, forward, up);
    }

    void
    roll(double angle)
    {
        rotate_about(angle, forward, up, along);    
    }

    void
    walk(double d)
    {
        move_along(d, forward);
    }
    
    void
    strafe(double d)
    {
        move_along(d, along);
    }
    
    void
    fly(double d)
    {
        move_along(d, up);
    }
    
};

#endif

