/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __GTS_VOXELIZATION_H__
#define __GTS_VOXELIZATION_H__

#include <gts.h>

// GTS Voxelization
//  Voxelization of polygon meshes.
//
//  Note:
//   This is just a wrapper class to enable the filling procedures to use the
//   gts library inside/outside mesh functionality.
class gts_voxelization
{
private:
    GNode *t;

    // Privatize until implemented.
    gts_voxelization(const gts_voxelization& v){}

public:
    gts_voxelization() : t(0) {}
    gts_voxelization(GtsSurface *s) : t(gts_bb_tree_surface(s)){}
    ~gts_voxelization()
    {
        if(t) gts_bb_tree_destroy(t, true);
    }

    template<class POINT_TYPE>
    int
    classify(const POINT_TYPE* p) const
    {
        GtsPoint q;
        q.x = p[0]; q.y = p[1]; q.z = p[2];
        return gts_point_is_inside_surface(&q, t, false)? 1: 0;
    }
};

#endif

