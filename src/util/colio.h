/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __COLIO_H__
#define __COLIO_H__

#include <fstream>
#include <ostream>
#include <sstream>
#include <vector>

template<class FLOAT_TYPE>
void
load_col_file(const std::string &probability_file,
    std::vector<std::vector<FLOAT_TYPE> > &probabilities)
{
    std::ifstream in(probability_file.c_str());
    std::string line;
    while(std::getline(in, line))
    {
        // Use double here to ensure we can read double size values
        double value;

        std::stringstream ss(line);
        probabilities.push_back(std::vector<FLOAT_TYPE>());
        while(ss >> value) probabilities.back().push_back(value);
    }
}

template<class FLOAT_TYPE>
void
save_col_file(const std::string &probability_file,
    const std::vector<std::vector<FLOAT_TYPE> > &probabilities)
{
    std::ofstream out(probability_file.c_str());
    for(int i = 0; i < probabilities.size(); i++)
    {
        const std::vector<FLOAT_TYPE> &column = probabilities[i];
        for(int j = 0; j < column.size()-1; j++) out << column[j] << " ";
        out << column.back() << std::endl;
    }
}

#endif

