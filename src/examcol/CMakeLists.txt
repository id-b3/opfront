add_executable(examcol main.cpp)

target_link_libraries(examcol ${Boost_LIBRARIES})
target_link_libraries(examcol ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(examcol ${CImg_SYSTEM_LIBS})

if(NiftiCLib_FOUND)
    target_link_libraries(examcol ${NiftiCLib_LIBRARIES})
endif(NiftiCLib_FOUND)

install(TARGETS examcol DESTINATION bin)

