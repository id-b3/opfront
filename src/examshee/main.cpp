/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"

namespace po = boost::program_options;

int
level2res(int level)
{
    int r = 2;
    for(int i = 2; i <= level; i++) r = r*2-1;
    return r;
}

void
generate_warp(int signal_res, int ground_truth_res,
    cimg_library::CImg<double> &warp)
{
    int r = 2;
    warp.assign(2, 2, 1, 1, 0.5);
    warp.noise(0.5);
    for(int i = 2; i <= ground_truth_res; i++)
    {
        r = r*2-1;
        warp.resize(r, r, 1, 1, 5);
        if(i <= signal_res) warp.noise(1.0/warp.width());
    }
    warp -= warp.min();
    warp *= r/static_cast<double>(warp.max());
}


void
generate_bellshape(cimg_library::CImg<double> &warp)
{
    float c[] = {1.0, 1.0, 1.0};
    warp.draw_gaussian(static_cast<float>(warp.width()/2),
        static_cast<float>(warp.height()/2),
        static_cast<float>(warp.depth()/2),
        static_cast<float>(warp.width()/5), c);
    warp *= warp.width()/2;
    warp += warp.width()/4;
}

int
idx(int x, int y, int res)
{
    return x*res+y+1;
}
        
int
edx(int x0, int y0, int x1, int y1, int res)
{
    if(x0 < res-1)
    {
        if(y0 < res-1)
        {
            int b = ((res-1)*x0+y0)*3+1;
            if(x1 == x0) return b+2;
            else if(y1 == y0) return b;
            else return b+1;
        }
        else return (res-1)*(res-1)*3+x0+1;
    }
    else return (res-1)*(res-1)*3+res+y0;
}

void
write_edges_triangles_to_file(std::ofstream &out, int res)
{
    // Edges
    for(int x = 0; x < res-1; x++)
        for(int y = 0; y < res-1; y++)
    {
        out << idx(x, y, res) << " " << idx(x+1, y, res) << std::endl;
        out << idx(x, y, res) << " " << idx(x+1, y+1, res) << std::endl;
        out << idx(x, y, res) << " " << idx(x, y+1, res) << std::endl;
    }
    for(int x = 0; x < res-1; x++)
        out << idx(x, res-1, res) << " " << idx(x+1, res-1, res) << std::endl;
    for(int y = 0; y < res-1; y++)
        out << idx(res-1, y, res) << " " << idx(res-1, y+1, res) << std::endl;

    // Triangles
    for(int x = 0; x < res-1; x++) for(int y = 0; y < res-1; y++)
    {
        out << edx(  x,   y, x+1,   y, res) << " " <<
            edx(x+1,   y, x+1, y+1, res) << " " <<
            edx(  x,   y, x+1, y+1, res) << std::endl;
        out << edx(  x,   y, x+1, y+1, res) << " " <<
            edx(  x,   y,   x, y+1, res) << " " <<
            edx(  x, y+1, x+1, y+1, res) << std::endl;
    }
}

double
factor(int p, int scale)
{
    if(!p || p == scale) return 0.5;
    else return 1.0;
}

double
avg_neighbourhood(cimg_library::CImg<double> &prob, int x, int y, int z,
    int scale)
{
    if(scale == 1) return prob(x, y, z);
    double c = 0;
    double s = 0;
    for(int i = 0; i <= scale; i++)
        for(int j = 0; j <= scale; j++)
            for(int k = 0; k <= scale; k++)
    {
        int xi = x+i-scale/2, yi = y+j-scale/2, zi = z+k-scale/2;
        double f = factor(i, scale)*factor(j, scale)*factor(k, scale);
        //std::cout << x << " " << y << " " << z << " - " << xi << " " << yi << " " << zi << " - " << scale << std::endl;
        if(xi < 0 || xi > prob.width()-1 ||
           yi < 0 || yi > prob.height()-1 ||
           zi < 0 || zi > prob.depth()-1) continue;
        s += f*prob(xi, yi, zi);
        c += f;
        
    }
    return s/c;
}

int
main(int ac, char* av[])
{
    try
    {
        int graph_res, ground_truth_res, signal_res, seed;
        double noise, sigma;
        std::string out_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("ground_truth_resolution,r",
                po::value<int>(&ground_truth_res)->default_value(7),
                "resolution (level) of ground truth")
            ("signal_resolution,R",
                po::value<int>(&signal_res)->default_value(5),
                "resolution (level) of signal")
            ("graph_resolution,g",
                po::value<int>(&graph_res)->default_value(5),
                "resolution of graph (level)")
            ("noise,n", po::value<double>(&noise)->default_value(0.1),
                "noise amplitude")
            ("seed,S", po::value<int>(&seed)->default_value(0),
                "random seed (used for noise)")
            ("smooth,s", po::value<double>(&sigma)->default_value(1),
                "smoothing standard deviation")
            ("out_prefix,o", po::value<std::string>(&out_prefix),
                "the output prefix");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Example sheet generator" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        assert(ground_truth_res >= signal_res);
        assert(signal_res >= 2);
        int res = level2res(ground_truth_res);
        // Do random example image with region cost.
        cimg_library::cimg::srand(seed);
        cimg_library::CImg<double> surfaces(res, res, res, 1, 0), warp;
        generate_warp(signal_res, ground_truth_res, warp);
        if(signal_res < 0) generate_bellshape(warp);
        cimg_forXY(warp, x, y)
        {
            for(int z = 0; z < res; z++)
            {
                // The surface partially overlaps with voxels -0.5 to 0.5 in
                // each direction. E.g. if we have a surface at z = 1.75 then
                // 0.25 of the voxel at z = 2 is inside the surface so to get
                // the correct probability we set it to 1.75-2+0.5 = 0.25.
                surfaces(x, y, z) = warp(x, y)-z+0.5;

                if(signal_res >= 0)
                {
                    if(surfaces(x, y, z) > 1) surfaces(x, y, z) = 1;
                    if(surfaces(x, y, z) < 0) surfaces(x, y, z) = 0;
                }
            }
        }
        //int sr = level2res(signal_res);
        cimg_library::CImg<double> noisy(res, res, res, 1, 0);
        noisy.blur(sigma);
        noisy.noise(noise);
        //noisy.resize(res, res, res, 1, 5);
        noisy += surfaces;
        warp.save("warp.nii");
        surfaces.save((out_prefix+"ground_truth.nii").c_str());
        /*cimg_forXYZ(noisy, x, y, z)
        {
            if(noisy(x, y, z) > 1) noisy(x, y, z) = 1;
            else if(noisy(x, y, z) < 0) noisy(x, y, z) = 0;
        }*/
        noisy.save((out_prefix+"noisy.nii").c_str());

        std::ofstream gt((out_prefix+"ground_truth.gts").c_str());
        gt << res*res << " " << 2*(res-1)+(res-1)*(res-1)*3 << " " <<
            (res-1)*(res-1)*2  << " GtsSurface GtsFace GtsEdge GtsVertex" <<
            std::endl;
        for(int x = 0; x < res; x++)
            for(int y = 0; y < res; y++)
        {
                gt << x << " " << y << " " << warp(x, y) << std::endl;
        }
        write_edges_triangles_to_file(gt, res);

        std::ofstream initial((out_prefix+"initial.gts").c_str()),
            pos((out_prefix+"column.col").c_str()),
            prob((out_prefix+"region.prop").c_str());
        int sres = level2res(graph_res);
        initial << sres*sres << " " << 2*(sres-1)+(sres-1)*(sres-1)*3 << " " <<
            (sres-1)*(sres-1)*2  << " GtsSurface GtsFace GtsEdge GtsVertex" <<
            std::endl;
        /*for(int x = 0; x < res; x += scale)
            for(int y = 0; y < res; y += scale)
        {
            for(int z = 0; z < res; z += scale)
            {
                pos << x << " " << y << " " << z;
                prob << noisy(x, y, z);
                if(z+scale < res)
                {
                    pos << " ";
                    prob << " ";
                }
            }
            pos << std::endl;
            prob << std::endl;
        }
        for(int x = 0; x < res; x += scale)
            for(int y = 0; y < res; y += scale)
        {
            for(int z = 0; z < res; z += scale)
            {
                pos << x+scale/2.0-0.5 << " " << y+scale/2.0-0.5 << " "
                    << z+scale/2.0-0.5;
                prob << avg_neighbourhood(noisy, x, y, z, scale);
                if(z+scale < res)
                {
                    pos << " ";
                    prob << " ";
                }
            }
            pos << std::endl;
            prob << std::endl;
        }*/
        int delta = (res-1)/(sres-1);
        assert(!((res-1)%(sres-1)));
        for(int i = 0; i < sres; i++)
            for(int j = 0; j < sres; j++)
        {
            int x = i*delta, y = j*delta;
            initial << x << " " << y << " " << 0 << std::endl;
            for(int k = 0; k < sres; k++)
            {
                int z = k*delta;
                pos << x << " " << y << " " << z;
                //prob << avg_neighbourhood(noisy, x, y, z, delta);
                prob << noisy(x, y, z);
                if(k < sres-1)
                {
                    pos << " ";
                    prob << " ";
                }
            }
            pos << std::endl;
            prob << std::endl;
        }       
        write_edges_triangles_to_file(initial, sres);
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

