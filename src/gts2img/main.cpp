/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/gts_voxelization.h"
#include "util/fill.h"
#include "util/gts_meta.h"

namespace po = boost::program_options;

gint
generate_seeds(gpointer item, gpointer data)
{
    GtsPoint *p = GTS_POINT(item);
    std::vector<float> *seeds = static_cast<std::vector<float>*>(data);
    seeds->push_back(p->x);
    seeds->push_back(p->y);
    seeds->push_back(p->z);
    return 0;
}

int
main(int ac, char* av[])
{
    try
    {
        int sub_sample;
        std::string volume_file, segmentation_file, gts_file;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("gts_file,g", po::value<std::string>(&gts_file),
                "the input gts file")
            ("sub_sample,u", po::value<int>(&sub_sample)->default_value(3),
                "sub sample the voxels to get partial volume effects")
            ("img_file,v", po::value<std::string>(&volume_file),
                "base dimensions on this")
            ("segmentation_file,s", po::value<std::string>(&segmentation_file),
                "the output segmentation file");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || segmentation_file == "" || gts_file == "")
        {
            std::cout << "GTS to image" << std::endl;
            std::cout << "Version 1.20 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
#ifdef OPFRONT_DEBUG
        std::cout << "Loading volume file: " << volume_file << " "
                  << std::flush;
#endif
        cimg_library::CImg<float> segmentation(volume_file.c_str());
#ifdef OPFRONT_DEBUG
        std::cout << "done!" << std::endl;
        std::cout << "Loading gts file: " << gts_file << " " << std::flush;
#endif
        GtsSurface *surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(), gts_vertex_class());
        FILE *pfile = fopen(gts_file.c_str(), "r");
        GtsFile *fp = gts_file_new(pfile);
        int err = gts_surface_read(surface, fp);
        gts_file_destroy(fp);
        fclose(pfile);
        gts_meta_ijk2xyz(segmentation, surface);
#ifdef OPFRONT_DEBUG
        std::cout << "done!" << std::endl;
        std::cout << "Filling voxels... " << std::flush;
#endif
        std::vector<float> seeds;
        gts_voxelization voxelizer(surface);
        fill<float, float, gts_voxelization> filler(&voxelizer);
        gts_surface_foreach_vertex(surface, generate_seeds,
            static_cast<gpointer>(&seeds));
        segmentation.fill(0);
        filler.grow_sub(segmentation, &seeds[0], seeds.size(), sub_sample);
#ifdef OPFRONT_DEBUG
        std::cout << "Done!" << std::endl;
        std::cout << "Saving to: " << segmentation_file << " " << std::flush;
#endif
        segmentation.save(segmentation_file.c_str());
#ifdef OPFRONT_DEBUG
        std::cout << "done!" << std::endl;
#endif
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

