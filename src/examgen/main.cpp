/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/lalg.h"

namespace po = boost::program_options;

void
draw_tube(cimg_library::CImg<float> &img,
    float *end_points,
    float radius,
    int value)
{
    cimg_forXYZ(img, x, y, z)
    {
        float x0[] = {static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)};
        float d = linalg_dsq_seg_ve3(x0, end_points, end_points+3);
        if(d <= radius*radius) img(x, y, z) = value;
    }
}

void
draw_sphere(cimg_library::CImg<float> &img,
    float *centre,
    float radius,
    int value)
{
    cimg_forXYZ(img, x, y, z)
    {
        float x0[] = {static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)};
        float d = linalg_dsq_ve3(x0, centre);
        if(d <= radius*radius)
        {
            img(x, y, z) = value;
        }
    }
}

int
main(int ac, char* av[])
{
    try
    {
        float noise, sigma;
        std::string out_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("noise,n", po::value<float>(&noise)->default_value(1),
             "noise amplitude")
            ("smooth,s", po::value<float>(&sigma)->default_value(1),
             "smoothing standard deviation")
            ("out_prefix,o", po::value<std::string>(&out_prefix),
             "the output prefix");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Example generator" << std::endl;
            std::cout << "Version 1.00 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        // Do random example image with probability files and initial
        // segmentation.
        int res = 200, bound = res/10;
        float inner = res/25.0, outer = res/20.0;
        cimg_library::CImg<float>
            example(res, res, res, 1, 0.5),
            interior(res, res, res, 1, 0),
            wall(res, res, res, 1, 0),
            initial(res, res, res, 1, 0);
        example.metadata.origin[0] = -50;
        example.metadata.origin[1] = 50;
        example.metadata.origin[2] = 0;
        example.metadata.spacing[0] = 0.75;
        example.metadata.spacing[1] = 1;
        example.metadata.spacing[2] = 0.5;
        interior.set_metadata(example);
        wall.set_metadata(example);
        initial.set_metadata(example);
        float l0[] = {static_cast<float>(res/2), static_cast<float>(res/2), static_cast<float>(bound),
                      static_cast<float>(res/2), static_cast<float>(res/2), static_cast<float>(res/2)},
            l1[] = {static_cast<float>(res/2), static_cast<float>(res/2), static_cast<float>(res/2),
                    static_cast<float>(3*bound), static_cast<float>(res/2), static_cast<float>(res-bound)},
            l2[] = {static_cast<float>(res/2), static_cast<float>(res/2), static_cast<float>(res/2),
                    static_cast<float>(res-3*bound), static_cast<float>(res/2), static_cast<float>(res-bound)};
        draw_tube(interior, l0, inner, 1);
        draw_tube(interior, l1, inner, 1);
        draw_tube(interior, l2, inner, 1);
        draw_tube(wall, l0, outer, 1);
        draw_tube(wall, l1, outer, 1);
        draw_tube(wall, l2, outer, 1);
        example.mul(1-wall);
        wall -= interior;
        example += wall;
        example.noise(noise, 0);
        example.blur(sigma, sigma, sigma);
        example.save((out_prefix+"noisy.nii.gz").c_str());
        std::cout << "Initial seed sphere: ("
                  << res/2 << ", " << res/2 << ", " << bound
                  << ") and radius: " << inner << std::endl;
        float s0[] = {static_cast<float>(res/2), static_cast<float>(res/2), static_cast<float>(bound)};
        draw_sphere(initial, s0, inner, 1);
        initial.save((out_prefix+"initial.nii.gz").c_str());
        interior = 1-(example/0.5).pow(2);
        wall = 1-((example-1)/0.5).pow(2);
        cimg_forXYZ(interior, x, y, z)
        {
            if(interior(x, y, z) > 1) interior(x, y, z) = 1;
            if(interior(x, y, z) < 0) interior(x, y, z) = 0;
            if(wall(x, y, z) > 1) wall(x, y, z) = 1;
            if(wall(x, y, z) < 0) wall(x, y, z) = 0;
        }
        interior.save((out_prefix+"interior_probability.nii.gz").c_str());
        wall.save((out_prefix+"wall_probability.nii.gz").c_str());
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

