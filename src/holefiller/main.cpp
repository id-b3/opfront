/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include "cimgext/cimgext.h"
#include <boost/program_options.hpp>
#include "util/cimg_util.h"


namespace po = boost::program_options;

std::string input_file, output_file;

template<class CIMG_TYPE>
void
process()
{
    std::cout << "Loading segmentation file..." << std::flush;
    cimg_library::CImg<CIMG_TYPE> segmentation(input_file.c_str());
    std::cout << " Done!" << std::endl;
    std::set<CIMG_TYPE> segments;
    cimg_forXYZ(segmentation, x, y, z)
    {
        if(segmentation(x, y, z)) segments.insert(segmentation(x, y, z));
    }
    for(typename std::set<CIMG_TYPE>::iterator i = segments.begin();
        i != segments.end(); i++)
    {
        std::cout << "Processing segment: " << static_cast<int>(*i)
            << std::endl;
        std::cout << " Removing tunnels in segmentation... " << std::flush;
        int c = remove_tunnels<CIMG_TYPE>(segmentation, 3, 0, *i);
        std::cout << c << " voxels was filled!" << std::endl;
        std::cout << " Removing holes in segmentation... " << std::flush;
        c = remove_holes<CIMG_TYPE>(segmentation, 0, *i);
        std::cout << c << " voxels was filled!" << std::endl;
    }
    std::cout << "Saving output..." << std::flush;
    segmentation.save(output_file.c_str());
    std::cout << " Done!" << std::endl;
}

int
main(int ac, char* av[])
{
    try
    {
        std::string datatype;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("input_file,i", po::value<std::string>(&input_file),
                "the input segmentation file")
            ("output_file,o", po::value<std::string>(&output_file),
                "the output segmentation file")
            ("datatype,d", po::value<std::string>(&datatype)->
                default_value("uchar"), "uchar, short, float, double");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || input_file == "" || output_file == "")
        {
            std::cout << "Fill holes and tunnels in segmentation" << std::endl;
            std::cout << "Version 1.10 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(datatype == "uchar")
        {
            process<unsigned char>();
        }
        else if(datatype == "short")
        {
            process<short>();
        }
        else if(datatype == "float")
        {
            process<float>();
        }
        else if(datatype == "double")
        {
            process<double>();
        }
        else
        {
            std::cerr << "ERROR: Unsupported datatype!" << std::endl;
            return 1;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

