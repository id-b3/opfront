/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/gts_mcube.h"
#include "util/cimg_util.h"
#include "util/gts_util.h"
#include "util/statistics.h"

namespace po = boost::program_options;

bool zero_boundary;
double img_value;
std::string img_file, gts_file;

template<class CIMG_TYPE>
void
process()
{
    std::cout << "Loading image file: " << img_file << std::flush;
    cimg_library::CImg<CIMG_TYPE> img(img_file.c_str());
    int ox = img.width(),
        oy = img.height(),
        oz = img.depth();
    cimg_forXYZ(img, x, y, z)
    {
        if(img(x, y, z) > img_value) img(x, y, z) = 0;
        else img(x, y, z) = 1;
    }
    if(zero_boundary)
    {
        cimg_forXYZ(img, x, y, z)
        {
            if(!x || x == img.width()-1 ||
               !y || y == img.height()-1 ||
               !z || z == img.depth()-1)
                img(x, y, z) = 1;
        }
    }
    std::cout << " done!" << std::endl;
    std::cout << "Running marching cubes... " << std::flush;
    GtsSurface *surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(), gts_vertex_class());
    gts_mcube_grid(surface, img);
    std::cout << "Done!" << std::endl;
    GtsSurfaceQualityStats stat;
    gts_surface_quality_stats(surface, &stat);
    std::cout << "Edges: " << stat.edge_length.n << " ["
        << stat.edge_length.min << ", " << stat.edge_length.max
        << "] " << stat.edge_length.mean << std::endl;
    std::cout << "Surface is closed: " << gts_surface_is_closed(surface)
        << ", is a manifold: " << gts_surface_is_manifold(surface)
        << ", is oriented: " << gts_surface_is_orientable(surface)
        << std::endl;
    std::cout << "Saving gts file: " << gts_file << std::flush;
    FILE *pfile = fopen(gts_file.c_str(), "w");
    gts_surface_write(surface, pfile);
    fclose(pfile);
    std::cout << " done!" << std::endl;
    gts_object_destroy(GTS_OBJECT(surface));
    gts_finalize();
}

int
main(int ac, char* av[])
{
    try
    {
        std::string datatype;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("img_file,s", po::value<std::string>(&img_file),
                "the input img file")
            ("image_value,v",
                po::value<double>(&img_value)->default_value(0),
                "the voxel value to process (bigger assumed inside)")
            ("datatype,d", po::value<std::string>(&datatype)->
                default_value("uchar"), "uchar, float")
            ("gts_file,g", po::value<std::string>(&gts_file),
                "the output gts file")
            ("zero_boundary,b",
                po::value<bool>(&zero_boundary)->default_value(true),
                "Set the image boundary to be empty");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || img_file == "" || gts_file == "")
        {
            std::cout << "Image to gts file" << std::endl;
            std::cout << "Version 1.20 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(datatype == "char")
        {
            process<char>();
        }
        else if(datatype == "uchar")
        {
            process<unsigned char>();
        }
        else if(datatype == "short")
        {
            process<short>();
        }
        else if(datatype == "float")
        {
            process<float>();
        }
        else if(datatype == "double")
        {
            process<double>();
        }
        else
        {
            std::cerr << "ERROR: Unsupported datatype!" << std::endl;
            return 1;
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

