FROM ubuntu:focal as builder

# Prepare cmake
ENV TZ=Europe/Amsterdam
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install -y cmake build-essential
WORKDIR /opfront

# -----------------------------------------
# DEPENDENCIES

## 1. Prepare Boost
#RUN mkdir -p ~/dev/boost && \
#        cd ~/dev/boost && \
#        wget -nv https://dl.bintray.com/boostorg/release/1.76.0/source/boost_1_76_0.tar.gz && \
#        tar xzvf boost_1_76_0.tar.gz && \
#        cd boost_1_76_0 && \
#        ./bootstrap.sh --with-libraries=program_options,timer && \
#        ./b2 -j16 install

# 2. Prepare NiftiCLib, GTS, CImg
RUN apt-get install -y --no-install-recommends libboost-all-dev libgts-dev libnifti-dev libsdl2-dev libsdl2-2.0 wget unzip

# CIMG
RUN wget -nv https://github.com/dtschump/CImg/archive/refs/tags/v.179.zip && \
        unzip -d /opfront/thirdparty v.179.zip && \
        mv /opfront/thirdparty/CImg-v.179/CImg.h /usr/include/CImg.h

# RUN git clone https://github.com/NIFTI-Imaging/nifti_clib.git
# RUN wget -nv https://deac-ams.dl.sourceforge.net/project/niftilib/nifticlib/nifticlib_2_0_0/nifticlib-2.0.0.tar.gz && tar -xf nifticlib-2.0.0.tar.gz
# RUN wget -nv https://versaweb.dl.sourceforge.net/project/niftilib/nifticlib/nifticlib-1.1.0/nifticlib-1.1.0.tar.gz && tar -xf nifticlib-1.1.0.tar.gz
# WORKDIR /opfront/nifticlib-1.1.0
# RUN cmake \
#         -DBUILD_SHARED_LIBS=ON \
#         -DNIFTI_USE_PACKAGING=ON \
#         -DUSE_CIFTI_CODE=OFF \
#         -DUSE_FSL_CODE=OFF \
#         -DNIFTI_BUILD_APPLICATIONS=ON \
#         -DTEST_INSTALL=OFF \
#         -DDOWNLOAD_TEST_DATA=OFF \
#         . \
#         && make install

# 5. Prepare MAXFLOW
#RUN wget -nv pub.ist.ac.at/~vnk/software/maxflow-v3.04.src.zip && unzip -d /opfront/thirdparty maxflow-v3.04.src.zip

# DEBUGGER INSTALL - REMOVE PRE-Production
# RUN apt-get install -y gdb

# ----------------------------------------
# Copy source and compile

COPY ./src ./src
COPY ./thirdparty ./thirdparty
WORKDIR /opfront/bin
RUN cmake /opfront/src 

# ----------------------------------------
# Create the final image and install the opfront tools
# 
# FROM debian:buster-slim
# RUN apt-get update && apt-get -y install build-essential cmake
# COPY --from=builder /opfront/ /opfront/
# WORKDIR /opfront/bin
RUN make install
# 
ENTRYPOINT ["/bin/bash"]
