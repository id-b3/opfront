# Opfront - Optimal front segmentation of images

  [Opfront](https://bitbucket.org/opfront)
  Jens Petersen (phup@diku.dk)

## Introduction

--------------------
This software provides functionality to segment three-dimensional images with
repeated use of optimal surfaces. The result is moving fronts, which adapts to
structures within the images. The approach uses flow line based columns as
described in:

  J. Petersen, M. Nielsen, P. Lo, L. H. Nordenmark, J. H. Pedersen,
  M. M. Wille, A. Dirksen, M. de Bruijne, "Optimal surface segmentation
  using flow lines to quantify airway abnormalities in chronic obstructive
  pulmonary disease", Medical Image Analysis, 2014, 18(3), 531-41.

If you decide to use this software, you should cite the above paper.

## Overview of the software

--------------------
The folders to be concerned about are:  
  **src** - All source code for the project

The folders for the main applications are:  
  ->**segsurf** - Segment using a single moving front.  
  ->**segwall** - Segment using a double moving front.  
  ->**seg4wda** - Segment using a double moving front across multiple registered  
              images.  
  ->**examgen** - Generate example images to work with.  
  ->**img2gts** - Convert images in nii-format to meshes in gts-format.  
  ->**gts2img** - Convert meshes in gts-format to images in nii-format.  
  ->**holefiller** - Fill holes (and tunnels) in segmentations stored as images.  
  ->**flowextr** - Extract flow lines to file.  
  ->**grphsolv** - Functionality to solve an optimal surface graph.  

Some folders with useful functionality:  
  ->**optisurf** - Optimal surface segmentation.  
  ->**opfront** - Optimal front segmentation.  
  ->**cimgext** - CImg extension to allow loading of nifti images (nii) with  
              metadata.  
  ->**util** - various utilities.  

## Dependencies

--------------------
The project as a whole has the following list of dependencies:  

[**CImg**](http://cimg.eu/) ([v1.7.9](https://github.com/dtschump/CImg/releases/tag/v.179))  
  Used for image i/o and processing. Note CImg supports a number of image
  formats, but may require additional libraries to do this, such as Medcon  
  (DICOM) and Imagemagick. Note at the moment Opfront only supports Nifti
  correctly (see Niftilib below).  

[**BOOST**](http://www.boost.org/) (v1.76.0)  
  Used only for program options and timer.  
  See Dockerfile for steps to install.  

[**GTS/GLIB**](http://gts.sourceforge.net/) (libgts-dev v0.7.6)  
  Used for meshing operations.  

[**MAXFLOW**](http://pub.ist.ac.at/~vnk/software.html) ([v3.04](http://pub.ist.ac.at/~vnk/software/maxflow-v3.04.src.zip))  
  Used to compute maximum flow/minimum cut.  

[**Niftilib/znzlib/zlib**](https://github.com/NIFTI-Imaging/nifti_clib) (v2.0.0.3)  
  *optional, strongly recommended*  
  Used for saving/loading nifti images. Note: image voxel spacing is only properly considered if using Nifti images and Opfronts behaviour is thus  
  currently uncertain for other image formats.  

[**SDL/GL**](https://www.libsdl.org/)  
  *optional, disabled by default*  
  Used for visualization debug runs, enabled by VISUALIZATION = YES in the appropriate makefile, but probably not useful for the average user.  

[**FFTW**](http://www.fftw.org/)  
  *optional, disabled by default*  
  Use libfftw for computing discrete fourier transforms. Enabled by FFTW = YES in the appropriate makefile.  

## Compilation

--------------------
All provided utilities are written in C++.

Compile and install binaries with CMake
    To do this, navigate to a folder where you want the binaries to be compiled
    (e.g. create a subfolder named bin) and type ```ccmake [folder_with_source]```
    once makefiles have been generated type ```make``` to compile the code and/or
    ```make install``` to compile and install the code.

    Note: for faster execution, change the CMAKE_BUILD_TYPE variable to Release

--------------------

To build the docker image, you must have [Docker](https://www.docker.com/get-started) installed.
Run the command ```docker build -t opfront .``` in the root directory.
Once the image has been built, run ```docker run -ti opfront``` to launch the tool.
In order to mount a directory with files for processing, refer to [Docker Volumes](https://docs.docker.com/storage/volumes/) documentation.

## Example usage

--------------------

1. Generate example images:  
    ```examgen```
2. Convert the generated initial segmentation image to a surface mesh:  
    ```img2gts -s initial.nii.gz -d float -g initial.gts```
3. Segment the inner surface from the initial surface mesh:  
    ```segsurf -v noisy.nii.gz -L interior_probability.nii.gz -s initial.gts -n 'y' -w 0.1```
4. Segment both wall surfaces from the initial surface mesh:  
    ```segwall -v noisy.nii.gz -L interior_probability.nii.gz -W wall_probability.nii.gz -s initial.gts -n 'y' -w 0.1```
5. Convert the segmented interior mesh surface to an image:  
    ```gts2img -g surface0.gts -v noisy.nii.gz -s surface0.nii.gz```
6. Convert the segmented exterior mesh surface to an image:  
    ```gts2img -g surface1.gts -v noisy.nii.gz -s surface1.nii.gz```

## License & disclaimer

--------------------
    Opfront - Optimal front segmentation
    Copyright (c) 2014, Jens Petersen
    All rights reserved.
    
    Opfront is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this software.  If not, see <http://www.gnu.org/licenses/>.

    Opfront has a range of dependencies and if you decide to use or
    modify it, it should be done in compliance with what ever licenses and
    copyright issues that apply for those dependencies.
